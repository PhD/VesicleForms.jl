# using StaticArrays

## setup
const nPoints = 5
const ds = 1 / (nPoints - 1)
const m = 0.

rs = zeros( nPoints )
φs = zeros( nPoints )

## boundary conditions
rs[1] = rs[end] = φs[1] = 0
φs[end] = π

## derivatives
function derivative( f, n )
    if n == 1
        return ( f[2] - f[1] ) / ds
    end
    return ( f[n] - f[n-1] ) / ds
end

## constraints
calc_arc_length() = derivative( rs, 1 )
function calc_area()
    return 2pi * calc_arc_length() * sum( rs ) * ds
end

function calc_volume()
    return pi * calc_arc_length() * sum( rs.^2 .* sin.(φs) ) * ds
end

function calc_bending_energy()
    inner = 2:nPoints-1
    return 2pi * calc_arc_length() * ds * (
        sum( ((n->derivative(φs,n)).(inner) .+ sin.(φs[inner]) ./ rs[inner] .- 2m).^2 .* rs[inner] ) +
        ( 2derivative(phs,1) - 2m )^2 * rs[1] +
        ( 2derivative(phs,nPoints) - 2m )^2 * rs[nPoints]
    )
end
