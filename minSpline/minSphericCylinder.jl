using Base.Test
using Suppressor
using Dierckx
using QuadGK
using NLopt
using Plots
gr()
# pgfplots()

## setup
function safe_integrate( func, from, to, guard = 1e-8 )
    start, ending = from + guard, to - guard
    return quadgk( func, start, ending )[1]
end

struct Vesicle{ F <: Real }
    spline::Dierckx.ParametricSpline
    area::F
    volume::F
    arc_length::F
    bend_energy::F
    spont_curvature::F
    bend_rigidity::F
    surface_tension::F
    osm_pressure::F
end
function Vesicle( spline, m, κ, Σ, P )
    arcLength = calc_arc_length(spline)
    Vesicle( spline
                , calc_area( spline, arcLength )
                , calc_volume( spline, arcLength )
                , arcLength
                , calc_bending_energy( spline, arcLength, m )
                , m
                , κ
                , Σ
                , P
    )
end
Vesicle( vesicle, spline ) = Vesicle( spline
                                        , vesicle.spont_curvature
                                        , vesicle.bend_rigidity
                                        , vesicle.surface_tension
                                        , vesicle.osm_pressure
                                    )

# calculate arc length
function calc_arc_length( spline )
    return safe_integrate( x->norm( derivative(spline,x) ) , spline.t[1], spline.t[end] )
end
# const arcLength = calc_arc_length( S )

# calculate curvature
function mean_curvature( spline, s )
    r = S(s)[1]
    dr, dz = derivative( spline, s )
    d2r, d2z = derivative( spline, s, nu = 2 )
    ds = norm( derivative( spline, s) )
    κ1 = ( -dz * d2r + d2z * dr ) / ds^3
    κ2 = dz / ( r * ds )
    return 0.5( κ1 + κ2 )
end

# calculate bending energy
function calc_bending_energy( spline, arcLength, m )
    return 2pi * arcLength * safe_integrate( function(x)
                                                r = spline(x)[1]
                                                M = mean_curvature( spline, x )
                                                return (M-m)^2 * r
                                             end
                                , spline.t[1], spline.t[end] )
end
# const Eb = calc_bending_energy( S, arcLength, m )

# calculate area
function calc_area( spline, arcLength )
    return 2pi * arcLength * safe_integrate( x->spline(x)[1], spline.t[1], spline.t[end] )
end
# const area = calc_area( S, arcLength )

# calculate volume
function calc_volume( spline, arcLength )
    return pi * arcLength * safe_integrate(
                        x->(spline(x)[1])^2 * sign(derivative( spline, x)[2])
                        , spline.t[1]
                        , spline.t[end]
                        )
end
# const volume = calc_volume( S, arcLength )

## build spheric cylinder as reference
const m = 0.0
const radius = 1.
const height = 4.
const ppr = 10 # points per radius
const rs = Vector{Float64}()
const zs = Vector{Float64}()

for φ = 0.:0.5pi/ppr:0.5pi
    push!( rs, radius * sin(φ) )
    push!( zs, radius * ( 1. - cos(φ) ) )
end
for z = radius+radius/ppr:radius/ppr:radius+height-radius/ppr
    push!( rs, radius )
    push!( zs, z )
end
for φ = 0.:0.5pi/ppr:0.5pi
    push!( rs, radius * cos(φ) )
    push!( zs, height + radius * ( 1. + sin(φ) ) )
end
const S = ParametricSpline([rs zs]')

# display spheric cylinder
function draw_contour( spline, yrange=nothing )
    arcs = collect(linspace(spline.t[1],spline.t[end],1000))
    sPoints = spline(arcs)
    plot(sPoints[2,:],sPoints[1,:],ylim=yrange) |> display
end
draw_contour( S, (0,2radius) )

## test initial vesicle to be the same
const initialVesicle = Vesicle( S, m, 1., -1.1, 1. )
@testset "Initial Vesicle" begin
    @test initialVesicle.arc_length ≈ pi * radius + height atol = 1e-3
    @test initialVesicle.volume ≈ 4pi / 3. * radius^3 + pi * radius^2 * height atol = 1
    @test initialVesicle.area ≈ 4pi * radius^2 + 2pi * radius * height atol = 1e-1
    @test initialVesicle.bend_energy ≈ 6pi atol = 1
end

## minimize bending energy of spherical cylinder
# function to minimize
function smally( minimizer, gradient, vesicle, area, volume, nKnots )
    rs = @view minimizer[1:nKnots]
    zs = @view minimizer[nKnots+1:end]
    newSpline = ParametricSpline( [rs zs]' )
    # vesicle = Vesicle( vesicle, newSpline )
    return calc_bending_energy( newSpline, calc_arc_length( newSpline ), vesicle.spont_curvature )
end

function r_smally( minimizer, grad::Vector, vesicle, area, volume, nKnots )
    rs = minimizer
    zs = vesicle.spline.c[2,:]
    newSpline = ParametricSpline( [rs zs]' )
    vesicle = Vesicle( vesicle, newSpline )
    return vesicle.bend_energy
end

function animation_smally( minimizer, vesicle, area, volume, nKnots )
    rs = @view minimizer[1:nKnots]
    zs = @view minimizer[nKnots+1:end]
    newSpline = ParametricSpline( [rs zs]' )
    draw_contour( newSpline, (0., 9.) )
    vesicle = Vesicle( vesicle, newSpline )
    return vesicle.bend_energy
end

function area_constraint( minimizer, gradient, area, nKnots )
    rs = @view minimizer[1:nKnots]
    zs = @view minimizer[nKnots+1:end]
    newSpline = ParametricSpline( [rs zs]' )
    return abs(calc_area( newSpline, calc_arc_length( newSpline ) ) - area)
end

function volume_constraint( minimizer, gradient, volume, nKnots )
    rs = @view minimizer[1:nKnots]
    zs = @view minimizer[nKnots+1:end]
    newSpline = ParametricSpline( [rs zs]' )
    return abs(calc_volume( newSpline, calc_arc_length( newSpline ) ) - volume)
end
function optimal_shape( vesicle, fitness=smally )
    area = vesicle.area
    volume = vesicle.volume
    zRange = ( -6.,12. )
    rRange = ( 0., 6. )
    n = size( vesicle.spline.c, 2 )
    searchRange = fill( rRange, 2n )
    @view(searchRange[n+1:end]) .= [zRange]
    # methods = (
    #             # :adaptive_de_rand_1_bin
    #             :adaptive_de_rand_1_bin_radiuslimited,
    #             # , :separable_nes
    #             # , :xnes
    #             # , :de_rand_1_bin
    #             # , :de_rand_2_bin
    #             # , :de_rand_1_bin_radiuslimited
    #             # , :de_rand_2_bin_radiuslimited
    #             # , :random_search
    #             # , :generating_set_search
    #             # , :probabilistic_descent
    # )
    # res = Vector{BlackBoxOptim.OptimizationResults}( length(methods) )
    # for ( i, method ) in enumerate( methods )
    #     res[i] = bboptimize(
    #         (var) -> fitness( var, vesicle, area, volume, n )
    #         , SearchRange = searchRange
    #         , MaxSteps = 20_000
    #         , TargetFitness = 0.0
    #         , Method = method
    #         , TraceMode = :silent
    #     )
    # end
    # @show fits = best_fitness.(res)
    # @show cands = best_candidate.(res)
    # minx = cands[ indmin(fits) ]
    # @show optimize( var->fitness(var, vesicle, area, volume, n),
    #                 initialVesicle.spline.c'[:],
    #                 BFGS()
    #                 # ; iterations = 20_000
    # )
    opt = Opt(:LD_MMA, 2n )
    min_objective!( opt, (p, grad) -> fitness( p, grad, vesicle, area, volume, n) )
    # inequality_constraint!( opt, (x,g)->area_constraint(x,g,area,n), 1e-4 )
    # inequality_constraint!( opt, (x,g)->volume_constraint(x,g,volume,n), 1e-4 )
    xtol_abs!(opt,1e-8)
    xtol_rel!(opt,1e-8)
    # ftol_abs!(opt,1e-8)
    # stopval!(opt,1e-10)
    maxeval!(opt, 20_000)
    maxtime!(opt, 40) # time in seconds
    _, minx, ret = call_optimizer( opt, abs.(vesicle.spline.c'[:]) )
    draw_contour( ParametricSpline( [minx[1:n] minx[n+1:2n]]' ) )
    return ret
end

function optimal_shape_rvar( vesicle, fitness=r_smally )
    area = vesicle.area
    volume = vesicle.volume
    rRange = ( 0., 6. )
    n = size( vesicle.spline.c, 2 )
    searchRange = fill( rRange, n )
    opt = Opt(:GN_ISRES, n )
    min_objective!( opt, (p, grad) -> fitness( p, grad, vesicle, area, volume, n) )
    lower_bounds!( opt, fill( rRange[1], n ) )
    upper_bounds!( opt, fill( rRange[2], n ) )
    xtol_abs!(opt,1e-4)
    xtol_rel!(opt,1e-4)
    # ftol_abs!(opt,1e-8)
    # stopval!(opt,1e-10)
    maxeval!(opt, 20_000)
    maxtime!(opt, 40) # time in seconds
    _, minx, ret = call_optimizer( opt, abs.(vesicle.spline.c[1,:]) )
    draw_contour( ParametricSpline( [minx vesicle.spline.c[2,:]]' ) )
    return ret
end

function call_optimizer( optObj, initialData )
    ( minf, minx, ret ) = NLopt.optimize( optObj, initialData )
    println( minf,": ", minx )
    return minf, minx, ret
end

## playground
# function rosenbrock(x)
#   return( sum( 100*( x[2:end] - x[1:end-1].^2 ).^2 + ( x[1:end-1] - 1 ).^2 ) )
# end
#
# open("bboCompare.txt","w") do io
#     out = @capture_out begin
#         # TODO: this doesn't work for some reason
#         compare_optimizers(rosenbrock; SearchRange = (-5.0, 5.0), NumDimensions = 10, MaxTime = 3.0)
#     end
#     write( io, out )
# end
