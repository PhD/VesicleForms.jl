using BenchmarkTools
using NamedTuples
using Suppressor
using VesicleForms
using OrdinaryDiffEq

cache = zeros( 9 )
cache2 = zeros( 9 )
root_cache = zeros( 6 )
tspan = (0.0,1.0)
function gas( n = 1e5 )
    return IdealGas( float(n), 300.0, 5e-26 )
end
test_sphere = copy( VesicleForms.critSphere )
push!( test_sphere.parameters, 1.2407 )
collect( gas() )
sphereShape = vesicle_to_shape( VesicleForms.sphere, tspan )
parameters = merge( merge( collect( test_sphere ), collect( gas() ) ),@NT(ζ = VesicleForms.partition_sum( sphereShape, gas() ) ) )
parameters0 = merge( merge( collect( test_sphere ), collect( gas(0) ) ),@NT(ζ = VesicleForms.partition_sum( sphereShape, gas(0) ) ) )
testState = [0.1, 0.01, parameters.p[3], 1.2, parameters.A, parameters.V, 0.0, 0.05, 2.1]
problem = ODEProblem( VesicleForms.shape_gas_Equations,
        VesicleForms.initialConditions_sg( parameters0 ),
        ( 0.0, 0.5 ),
        parameters0
    )
revProblem = ODEProblem( VesicleForms.shape_gas_Equations,
    VesicleForms.endConditions_sg( parameters0 ),
        ( 1.0, 0.5 ),
        parameters0
    )

## shape-gas-Equations
@benchmark VesicleForms.initialConditions_sg( parameters, 0.2 )
@benchmark VesicleForms.initialConditions_sg!( cache, parameters, 0.2 )
@benchmark VesicleForms.endConditions_sg!( cache, parameters, 0.2 )
@benchmark VesicleForms.shape_gas_Equations( cache, testState, parameters )

## gas_interactions
@benchmark VesicleForms.root_match!( cache, parameters, test_sphere, gas(), parameters.ζ, (0.,1.) )
output = @capture_out( @btime VesicleForms.matchShot( test_sphere, gas(1.), parameters.ζ, (0.,1.), true ) )
nt = @NT(u = [zeros(9) for i in 1:1002],t = zeros(1002))
struct UTSolution
    u::Vector{Vector{Float64}}
    t::Vector{Float64}
end
out = UTSolution( [zeros(9) for i in 1:4] , zeros(4) )
VesicleForms.matchShot!( out ,problem,revProblem, parameters0, false )
@btime VesicleForms.matchShot!( $nt,$problem,$revProblem, $parameters,true )
open("matchShot_benchmark.txt","w") do file
    write( file, output )
end
open("matchShot_benchmark.txt","a") do file
    write( file, output )
end
