it = 1e-3
ic = VesicleForms.initialConditionsAV( p, it )
ic0 = VesicleForms.initialConditionsAV( p )
cache = zeros(7)
ic[2] = 1e-2 * ( 1 - 0.1 )
VesicleForms._shapeEquations( cache, ic, p, it )
cache |> println
ic[2] = 1e-2 * ( 1 + 0.1 )
VesicleForms._shapeEquations( cache, ic, p, it )
cache |> println
##
J = zeros(7,7)
VesicleForms._shapeEquations_jac( J, ic, p, it )
J
##
J
ForwardDiff.jacobian( (y,x)->VesicleForms._shapeEquations(y,x,p), cache, ic )
J - ForwardDiff.jacobian( (y,x)->VesicleForms._shapeEquations(y,x,p), cache, ic )

## Benchmarks

@benchmark VesicleForms.shapeEquations( cache, ic, p, it )
@benchmark VesicleForms.initialConditions( 0.0, $p )
## Plots
using Plots
using VesicleForms
using OrdinaryDiffEq
# gr( fmt = :svg )
unicodeplots()
shapeEquations = ODEFunction( VesicleForms._shapeEquations
    ; jac = VesicleForms._shapeEquations_jac
    )
shapeProblem = ODEProblem( shapeEquations,
                    VesicleForms.initialConditionsAV,
                    (0.0,1.0),
                    collect( VesicleForms.critSphere )
                )
sol = solve( shapeProblem, Vern9(), adaptive = false, dt = 1e-3 )
tspan1=(0.0,0.005)
tspan2=(0.0,2.0)
tspan3=(0.975,1.0)

varplot = plot( sol, vars = 3, tspan=tspan2, denseplot=false
    # , ylims = (1.611991947,1.611991962)
)
# plot([ abs(x - 1) for x in 0:0.01:2])
##
p = length(sol)÷2
off = 100
show(IOContext(stdout,:compact => false), sol[3,:][p-off:p+off].-sol[3,1])
show(IOContext(stdout,:compact => false), sol[3,:])
sol.t |> show
plot( sol.t, sol[3,:] )

##
using OrdinaryDiffEq
function full_parameter_scan( vesicle, tspan = (0.0,1.0); points = 5, box = 0.1, kwargs... )
    parameters = vesicle.parameters
    p_range = range(parameters[1]*(1-box),(1+box)*parameters[1], length = points)
    Σ_range = range(parameters[2]*(1-box),(1+box)*parameters[2], length = points)
    u0_range = range(parameters[3]*(1-box),(1+box)*parameters[3], length = points)
    u1_range = range(parameters[4]*(1-box),(1+box)*parameters[4], length = points)
    S_range = range(parameters[5]*(1-box),(1+box)*parameters[5], length = points)
    it = Iterators.product( p_range, Σ_range, u0_range, u1_range, S_range )
    result = Array{ODESolution}(undef, length(it))
    for (i,p) in enumerate(it)
        vesicle.parameters .= p
        sol, Δu = VesicleForms.solve_shapeEquations( vesicle, tspan,
            callback = CallbackSet( VesicleForms.r_callback, VesicleForms.threepi_callback ),
         )
        result[i] = sol
    end
    return result
end # function
results = full_parameter_scan( vesicle, box = 0.7 )
if !all( res->res.retcode in VesicleForms.term_codes, results )
    global unstables = filter( res->res.retcode ∉ VesicleForms.term_codes, results )
    for unstable in unstables
        @info "Returned $(unstable.retcode)"
        @show unstable.prob.p
    end
end

## inspect Instability

using Plots
unstable = unstables[3]
for unstable in unstables
    vesicle.parameters .= unstable.prob.p.p
    global sol = VesicleForms.integrate_shapeEquations( vesicle, tspan .+ 1 .* (1e-3, -1e-3),
            callback = CallbackSet( VesicleForms.r_callback, VesicleForms.threepi_callback ),
     )
end
print(IOContext(stdout,:compact => false), collect( vesicle ))
scatter( sol.t, sol[2,:], xlims = (0,1),
    # ylims = (-0.1,0.1),
    markersize = 1.5,
    ylims = :auto
    )
