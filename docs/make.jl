using Documenter, VesicleForms

cp(joinpath(@__DIR__, "..", "README.md"), joinpath(@__DIR__, "src", "README.md"))
makedocs(
    sitename="VesicleForms.jl",
    authors = "Simon Christ",
    pages = [
        "README" => "README.md",
        "Overview" => "index.md",
        "workflows.md"
    ],
    format = Documenter.HTML(prettyurls = get(ENV, "CI", nothing) == "true"),
    repo = "https://gitlab.gwdg.de/PhD/VesicleForms.jl/blob/{commit}{path}#{line}"
)