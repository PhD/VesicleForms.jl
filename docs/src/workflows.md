# Workflows

## Traversing the morphology diagram

The morphology diagram is spanned by the variables `v` and `m̄`.
VesicleForms.jl provides three functions for exploring the morphology diagram: `scan_m`, `relax_to_v` and `change`.

### Varying spontaneous curvature `m̄`

```julia
using VesicleForms

tspan = ( 0.0, 1.0 )

vesicle0 = VesicleForms.prolate0_685 |> copy
nves = 500
m_max = 2
m_min = vesicle0.spont_curvature * sqrt( vesicle0.area / 4pi )
m_range = range( m_min, m_max, length = nves )
resicles = VesicleForms.scan_m( vesicle0,
        m_range;
        res_options = ( force_symmetry = false, dt = 1e-3 ),
        solver = :nlsolve,
        ftol = 1e-12,
        autodiff = :finite,
    )
res_vesicles = VesicleForms.solution_to_vesicle.(resicles)
```

### Varying normalized volume `v`

```julia
vesicle = copy( VesicleForms.prolate0_9 )
tspan = (0.0,1.0)

relax0_7 = relax_to_v( 
    copy(vesicle),
    0.7,
    tspan,
    -0.01,
    ftol = 1e-12
)
```

### Go either way with `change`

```julia
prolate0_8 = VesicleForms.change(
    copy(VesicleForms.prolate0_9),
    0.8,
    (0.0, 1.0),
    :v
)
prolates0_8 = [prolate0_8]
##
VesicleForms.change(
    copy(prolate0_8),
    1.5,
    (0.0, 1.0),
    :m̄,
    0.0002,
    store = prolates0_8,
)
```