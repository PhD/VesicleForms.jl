# VesicleForms.jl

This package provides routines to solve the shape equations for axi-symmetric liquid membranes.

```@contents
```

## Functions

```@autodocs
Modules = [VesicleForms]
Order = [:type, :function]
```

## Vesicle prototypes

```@eval
using VesicleForms, Plots, Markdown
vesicles_dict = VesicleForms.get_vesicles()
out = Markdown.MD()
for (vesicle, name) in vesicles_dict
    plot(vesicle, legend=:none)
    filename = string(name) * ".svg"
    savefig(filename)
    push!(out, Markdown.parse("### `VesicleForms.$name`: m̄ = $(round(vesicle.m̄, sigdigits = 4)), v = $(round(vesicle.v, sigdigits = 4))"))
    push!(out, Markdown.parse("![]($filename)"))
end
out
```

## Citing VesicleForms.jl

If you use `VesicleForms.jl` for academic research and wish to cite it,
please use the following paper.

Simon Christ, Thomas Litschel, Petra Schwille and Reinhard Lipowsky,
*Active shape oscillations of giant vesicles with
cyclic closure and opening of membrane necks*,
Soft Matter, 2021, **17**, 319

DOI: 10.1039/d0sm00790k

## Index

```@index
```