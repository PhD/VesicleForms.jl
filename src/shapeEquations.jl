function _shapeEquations_01(
    newState,
    state,
    parameters,
    time = nothing;
    rcut = 1e-2
)
    # @debug begin
    #     @assert (!any)(isinf.(state)) "$state is corrupted at t: $time"
    #     ""
    # end # debug
    pressure, Σ, v0, v1, S = parameters.p
    φ, r, v, γ = state
    m = parameters.m
    sinPhi, cosPhi = sincos(φ)
    sin2Phi = sinPhi * sinPhi
    Rve = sqrt(parameters.A / 4pi)
    if r < rcut * Rve
        # TODO: handle r small for arbitrary time correctly ( that is find t0 in 0 < t < 1 where r will be 0 )
        sinPhiByR_exp = function (u)
            return u +
                   1 / 6 *
                   (u - u^3 + 3 / 8 * (pressure + 2u * (2m^2 + Σ - 2m * u))) * ds^2
        end # function
        if cosPhi >= 0
            ds = S * time
            sinPhiByR = sinPhiByR_exp(v0)
        else
            ds = S * abs(time - 1)
            sinPhiByR = sinPhiByR_exp(v1)
        end # if
        if sinPhiByR - v < 1e-8 / Rve
            newState[3] = S * (γ * sinPhiByR + pressure * r * cosPhi / 2) # u = \dot{φ}
        else
            newState[3] = S *
                          (cosPhi / r * sinPhiByR + γ * sinPhiByR - v * cosPhi / r +
                           pressure * r * cosPhi / 2) # u = \dot{φ}
        end # if
        newState[4] = S *
                      (((v - 2m)^2 - sinPhiByR^2) / 2 + pressure * r * sinPhi + Σ) # γ
        newState[7] = S * pi * r * (v + sinPhiByR - 2m)^2 # bending Energy
    else
        newState[3] = S *
                      ((cosPhi * sinPhi / r + γ * sinPhi - v * cosPhi) / r +
                       pressure * r * cosPhi / 2) # u = \dot{φ}
        newState[4] = S *
                      (((v - 2m)^2 - sin2Phi / r^2) / 2 + pressure * r * sinPhi + Σ) # γ
        newState[7] = S * pi * r * (v + sinPhi / r - 2m)^2 # bending Energy
    end
    newState[1] = S * v
    newState[2] = S * cosPhi
    newState[5] = S * 2pi * r # A
    newState[6] = S * pi * sinPhi * r^2 # V
    return nothing
end

function _shapeEquations(newState, state, parameters, time = nothing; rcut = 1e-1)
    pressure, Σ, v0, v1, S = parameters.p
    φ, r, v, γ = state
    m = parameters.m
    sinPhi, cosPhi = sincos(φ)
    sin2Phi = sinPhi * sinPhi
    Rve = sqrt(parameters.A / 4pi)
    newState[1] = S * v
    newState[2] = S * cosPhi
    newState[3] = S *
                  ((cosPhi * sinPhi / r + γ * sinPhi - v * cosPhi) / r +
                   pressure * r * cosPhi / 2) # u = \dot{φ}
    newState[4] = S * (((v - 2m)^2 - sin2Phi / r^2) / 2 + pressure * r * sinPhi + Σ) # γ
    newState[5] = S * 2pi * r # A
    newState[6] = S * pi * sinPhi * r^2 # V
    newState[7] = S * pi * r * (v + sinPhi / r - 2m)^2 # bending Energy
    return nothing
end

# Jacobian
function _shapeEquations_jac(J, state, parameters, time = nothing; rcut = 1e-1)
    pressure, Σ, v0, v1, S = parameters.p
    m = parameters.m
    φ, r, v, γ = state
    Rve = sqrt(parameters.A / 4pi)

    sinPhi, cosPhi = sincos(φ)
    sin2Phi, cos2Phi = sincos(2φ)

    J[1, 1:2] .= 0
    J[1, 3] = S
    J[1, 4:6] .= 0
    J[2, 1] = -S * sinPhi
    J[2, 2:6] .= 0
    J[3, 5:6] .= 0
    J[4, 4:6] .= 0
    J[5, 1] = 0
    J[5, 2] = 2pi * S
    J[5, 3:6] .= 0
    J[6, 1] = S * pi * cosPhi * r^2
    J[6, 2] = S * 2pi * r * sinPhi
    J[6, 3:6] .= 0

    # if r < rcut * Rve
    #     J[3,1] = -S * 0.5pressure * r * sinPhi
    #     J[3,2] = S * 0.5pressure * cosPhi
    #     J[3,3] = -S * γ
    #     J[3,4] = S * v
    #     J[4,1] = S * cosPhi * pressure * r
    #     J[4,2] = S * sinPhi * pressure
    #     J[4,3] = S * ( - 2m )
    #     J[7,1] = 0.0
    #     J[7,2] = S * pi * ( 2v - 2m )^2
    #     J[7,3] = S * 4pi * r * ( 2v - 2m )
    # else
    J[3, 1] = S *
              (cos2Phi / r^2 - 0.5 * pressure * r * sinPhi +
               (sinPhi * v + cosPhi * γ) / r)
    J[3, 2] = S *
              (0.5pressure * cosPhi -
               (sin2Phi + r * (sinPhi * γ - cosPhi * v)) / r^3)
    J[3, 3] = -S * cosPhi / r
    J[3, 4] = S * sinPhi / r
    J[4, 1] = S * cosPhi / r^2 * (pressure * r^3 - sinPhi)
    J[4, 2] = S * sinPhi * (pressure + sinPhi / r^3)
    J[4, 3] = S * (v - 2m)
    J[7, 1] = S * 2pi * (v + sinPhi / r - 2m) * cosPhi
    J[7, 2] = S * pi *
              ((v + sinPhi / r - 2m)^2 - 2 * (v + sinPhi / r - 2m) * sinPhi / r)
    J[7, 3] = S * 2pi * r * (v + sinPhi / r - 2m)
    # end
    return nothing
end

const shapeEquations = ODEFunction(_shapeEquations
    # ; jac = _shapeEquations_jac
)

function initialConditions(t_start, parameters)
    pressure, tension, curvature_0, _, arcLength = parameters.p
    m = parameters.m

    γPrime_0 = 2m^2 - 2m * curvature_0 + tension
    γPrime2_0 = (4m^2 * curvature_0^2 - 3m^3 * curvature_0 +
                 curvature_0 / 4 * (9pressure + 2tension * curvature_0) -
                 m / 4 * (3pressure + 6tension * curvature_0 + 4curvature_0^3))
    dt = arcLength * t_start
    dt2 = dt^2 / 2
    dt3 = dt^3 / 6

    return [
        curvature_0 * dt + 3 / 8 * (pressure + 2γPrime_0 * curvature_0) * dt3,
        dt - dt3 * curvature_0^2,
        curvature_0 + 3 // 8 * (pressure + 2γPrime_0 * curvature_0) * dt2,
        γPrime_0 * dt + dt3 * γPrime2_0,
    ]
end

function initialConditionsAV(parameters, t_start = 0.0)
    m = parameters.m
    p, Σ, u0, _, S = parameters.p
    ds = S * t_start
    ds2 = ds^2 / 2
    ds3 = ds^3 / 6
    ds4 = ds^4 / 24
    return append!(
        initialConditions(t_start, parameters),
        [
         2pi * (ds2 - ds4 * u0^2),
         6pi * u0 * ds4,
         2pi *
         (2ds2 * (u0 - m)^2 +
          ds4 * (u0 - m) * (3p - 2u0 * (u0^2 + 5m * u0 - 3Σ - 6m^2)))
        ]
    )
end

function rightBoundary(t_start, parameters)
    pressure, tension, _, curvature_1, arcLength = parameters.p
    m = parameters.m

    γPrime_1 = 2m^2 - 2m * curvature_1 + tension
    γPrime2_1 = (4m^2 * curvature_1^2 - 3m^3 * curvature_1 +
                 1 // 4 * curvature_1 * (9pressure + 2tension * curvature_1) -
                 1 // 4 * m * (3pressure + 6tension * curvature_1 + 4curvature_1^3))
    ds = arcLength * t_start
    ds2 = ds^2 / 2
    ds3 = ds^3 / 6
    return [
        pi + curvature_1 * ds + 3 // 8 * (pressure + 2γPrime_1 * curvature_1) * ds3,
        -ds + ds3 * curvature_1^2,
        curvature_1 + 3 // 8 * (pressure + 2γPrime_1 * curvature_1) * ds2,
        γPrime_1 * ds + ds3 * γPrime2_1
    ]
end

function endConditionsAV(parameters, t_start = 0.0)
    p, Σ, _, u1, arcLength = parameters.p
    m = parameters.m
    area = parameters.A
    volume = parameters.V
    ds = arcLength * t_start
    ds2 = ds^2 / 2
    ds3 = ds^3 / 6
    ds4 = ds^4 / 24
    return append!(
        rightBoundary(t_start, parameters),
        [
         area - 2pi * (ds2 - u1^2 * ds4),
         volume - 6pi * u1 * ds4,
         -2pi *
         (2ds2 * (u1 - m)^2 +
          ds4 * (u1 - m) * (3p - 2u1 * (u1^2 + 5m * u1 - 3Σ - 6m^2)))
        ]
    )
end

function pi_condition(u, t, integrator, n = 1)
    u[1] - n * pi # terminating exactly at π gives strange results
end

function zero_condition(u, t, integrator)
    u[1]
end

function pi_terminate!(integrator)
    if 0 < integrator.u[2] < 2e-3
        return terminate!(integrator, :SweetSpot)
    end
    terminate!(integrator, :HitPi)
end

function r_condition(u, t, integrator)
    u[2]
end
function r_terminate!(integrator)
    if 0 < pi - integrator.u[1] < 5e-6
        return terminate!(integrator, :SweetSpot)
    end
    terminate!(integrator, :R0)
end

const shapeProblem = ODEProblem(
    _shapeEquations_01,
    initialConditionsAV,
    (0.0, 1.0),
    collect(sphere())
)
const r_callback = ContinuousCallback(
    r_condition,
    nothing,
    r_terminate!,
    save_positions = (true, false)
)
const pi_callback = ContinuousCallback(
    pi_condition,
    pi_terminate!,
    nothing,
    save_positions = (true, false),
    abstol = 1e-10
)
const twopi_callback = ContinuousCallback(
    (u, t, i) -> pi_condition(u, t, i, 2),
    x -> terminate!(x, :Hit2Pi),
    nothing,
    save_positions = (true, false)
)
const threepi_callback = ContinuousCallback(
    (u, t, i) -> pi_condition(u, t, i, 2.9), # shold trigger before r_callback
    x -> terminate!(x, :Hit3Pi),
    nothing,
    save_positions = (true, false),
)
const zero_callback = ContinuousCallback(
    zero_condition,
    nothing,
    x -> terminate!(x, :Hit0),
    save_positions = (true, false)
)
const term_codes = (:HitPi, :Hit2Pi, :Hit0, :R0, :Success, :Hit3Pi)
