module VesicleForms

using OrdinaryDiffEq
using LinearAlgebra
using RecursiveArrayTools
using NamedTupleTools
using OrderedCollections
using StaticArrays
using Parameters
using DocStringExtensions

# const alg = Vern9()  # explicit solver
const alg = Rodas4() # implicit solver
# const tols = (:reltol => 1e-8, :abstol => 1e-10)
const tols = (:reltol => 1e-10, :abstol => 1e-10)


export shapeEquations, endConditionsAV, initialConditionsAV
export Vesicle, sphere
export ODEsolve, ODEmatch, ODEshoot_opt, ODEshoot_root!,
    root_match!, root_residual!, root_residual2!, root_residualAV!, relax_to_v, vesicle_to_shape

import LinearAlgebra.norm
import Base.collect
import Base.print
import Base.length
import Base.copy
import Base.abs

abs( a::AbstractArray ) = map( abs, a )

include( "Vesicle.jl" )
include( "vesicle_prototypes.jl")
include( "shapeEquations.jl" )
include( "root_shooting_methods.jl" )
# include( "../_experimental/opt_shooting_methods.jl" )
# include( "../_experimental/shape_methods.jl")
include("parameterizations.jl")
include( "visualize.jl" )
include("util.jl")

end # VesicleForms
