struct Undoloid{F<:Real}
    r::Vector{F}
    z::Vector{F}
    ϕ::Vector{F}
    area::F
    volume::F
    m::F
end # struct

function Undoloid( r, z, ϕ, m = 0.0 )
    Undoloid{eltype(r)}( r, z, ϕ, int_area( r, z ), int_volume( r, z, ϕ ), m )
end # function

function int_area( r, z )
    area = ds = zero(eltype(r))
    for i in firstindex(r):lastindex(r)-1
        ds = sqrt( (r[i]-r[i+1])^2 + (z[i]-z[i+1])^2 )
        area += 2π * ds * r[i]
    end # if
    area += 2π * ds * r[end]
    return area
end # function

function int_volume( r, z, ϕ )
    volume = ds = zero(eltype(r))
    for i in firstindex(r):lastindex(r)-1
        ds = sqrt( (r[i]-r[i+1])^2 + (z[i]-z[i+1])^2 )
        volume += π * ds * r[i]^2 * sin(ϕ[i])
    end # if
    volume += π * ds * r[end]^2 * sin(ϕ[end])
    return volume
end # function

function int_volume( r, ϕ; ds = 1e-3 )
    volume = zero(eltype(r))
    for i in firstindex(r):lastindex(r)-1
        volume += π * ds * r[i]^2 * sin(ϕ[i])
    end # if
    volume += π * ds * r[end]^2 * sin(ϕ[end])
    return volume
end # function

function undoloid( R1::T, R2::T, a::T; ds = convert(T, 1e-3), m = zero(T) ) where T
    phi1s = 0:ds:pi/2
    ϕ = collect(phi1s)
    function dz( R, r, a )
        return 1 / (R+a) * ( r + R * a / r)
    end # function
    rs = T[ R1 * sin(phi) for phi in phi1s]
    zs = T[ R1 * (1 - cos(phi)) for phi in phi1s]
    r = last(rs)
    phi = last(phi1s)
    dphi = ds / (R1+a) * ( 1 - R1 * a / r^2 )
    phi += dphi
    push!( ϕ, phi )
    z = last( zs ) + ds * dz( R1, r, a )
    r = r + ds * cos(phi)
    while r > a + ds
        push!(zs, z)
        push!(rs, r)
        dphi = ds / (R1+a) * ( 1 - R1 * a / r^2 )
        phi += dphi
        push!( ϕ, phi )
        z = last( zs ) + ds * dz( R1, r, a )
        r = r + ds * cos(phi)
    end # for
    phi -= dphi
    pop!( ϕ )
    r = last(rs)
    dphi = ds / (R2+a) * ( 1 - R2 * a / r^2 )
    if a == 0
        phi = dphi
    else
        phi += dphi
    end # if
    push!( ϕ, phi )
    z = last( zs ) + ds * dz( R2, r, a )
    r = r + ds * cos(phi)
    while r < R2 - ds
        push!(zs, z)
        push!(rs, r)
        dphi = ds / (R2+a) * ( 1 - R2 * a / r^2 )
        phi += dphi
        push!( ϕ, phi )
        z = last( zs ) + ds * dz( R2, r, a )
        r = r + ds * cos(phi)
    end # for
    phi -= dphi
    pop!( ϕ )
    z = last(z)
    phi2s = pi/2:ds:pi
    append!(ϕ, collect(phi2s))
    append!(rs, T[ R2 * sin(phi) for phi in phi2s])
    append!(zs, T[ z - R2 * cos(phi) for phi in phi2s])

    return Undoloid( rs, zs, ϕ, m )
end # function
