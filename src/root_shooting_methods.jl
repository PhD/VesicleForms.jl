using LinearAlgebra
using RecursiveArrayTools
using OrdinaryDiffEq
using DiffEqBase
using NLsolve
using MINPACK
using ProgressMeter
using Formatting
using Lazy
using MDBM

"""
$(DocStringExtensions.SIGNATURES)
Integrate a given problem without callbacks.
"""
function solveProb( problem; alg = alg, tols = tols, dt = 1e-3, kwargs... )
    solution = solve( problem, alg;
                        tols...
                        , adaptive = false
                        , dt = dt
                        , kwargs...
                     )
    return solution
end

function integrate_shapeEquations( vesicle, tspan;
    ic = initialConditionsAV( collect(vesicle), tspan[1] ),
    rcut = 1e-2,
    alg = VesicleForms.alg,
    kwargs... )
    parameters = collect( vesicle )
    if tspan[1] == zero( tspan[1] )
        f = (du,u,p,t)-> _shapeEquations_01(du,u,p,t; rcut = rcut)
    else
        f = shapeEquations
    end # if
    problem = ODEProblem( f,
            ic,
            tspan,
            parameters
        )

    sol = solveProb( problem; kwargs... )
    return sol
end # function

"""
Solve the shape equations with fixed parameters.

$(DocStringExtensions.SIGNATURES)

## Positional arguments
 - `vesicle` : vesicle to start with
 - `tspan`   : the interval of the arclength to integrate over, typically (0.0, 1.0)

## Keyword arguments
 - `dt`      : stepsize for the integration (1e-3)
 - `rcut`    : value of the radial coordinate until which taylor expansions are used instead of integrating the shapeEquations (1e-2)
 - additional keywords are passed to `integrate_shapeEquations`

Returns `(sol::ODESolution, Δu)`, where `sol` is the augmented solution object returned from DifferentialEquations.jl and `Δu` is the difference of the endpoint of the integration and the taylor expansion around the true endpoint evaluated at the actual endpoint. 
"""
function solve_shapeEquations( vesicle, tspan; dt = 1e-3, rcut = 1e-2, kwargs...)
    t0 = tspan[1]
    parameters = collect( vesicle )
    ics = [ initialConditionsAV( parameters, t0 ) ]
    r0 = ics[1][2]
    while r0 < rcut*vesicle.R
        t0 += dt
        ic = initialConditionsAV( parameters, t0 )
        ic[2] <= 0 && break
        push!( ics, ic )
        r0 = ics[end][2]
    end # while
    ec = [ endConditionsAV( parameters, -t ) for t in t0:-dt:tspan[1] ]
    sol = integrate_shapeEquations( vesicle, ( t0, tspan[end] - t0 );
        dt = dt,
        ic = ics[end],
        rcut = rcut,
        kwargs... )
    Δu = sol.u[end] - ec[1]
    cache = similar( ics[1] )
    pre_ts = tspan[1]:dt:t0
    end_ts = (tspan[end]-t0):dt:tspan[end]
    pre_ks = [[(_shapeEquations_01(cache, ics[i], parameters, pre_ts[i]); cache) for i in 1:lastindex(ics)-1]]
    prepend!( sol.u, ics[1:end-1] )
    prepend!( sol.t, pre_ts[1:end-1] )
    prepend!( sol.k, pre_ks )
    append!( sol.u, ec[2:end] )
    append!( sol.t, end_ts[2:end] )
    append!( sol.k,
            [[(_shapeEquations_01(cache, ec[i], parameters, end_ts[i]); cache) for i in 2:lastindex(ec)]]
        )
    return sol, Δu
end

"""
$(DocStringExtensions.SIGNATURES)
Construct a shape by integrating the `shapeEquations` in the intervalls [ `tspan[1]`, `tspan[end÷2]` ] and [ `tspan[end]`, `tspan[end÷2]` ] with the parameters given by `collect( vesicle )`. Return `( u, t, prob )`.
"""
function matchShot( vesicle, tspan, save = true; dt = 1e-3, alg = VesicleForms.alg )
    parameters = collect( vesicle )

    problem = ODEProblem( _shapeEquations_01,
        eltype(parameters.p).(initialConditionsAV( parameters, tspan[1] )),
        eltype(tspan).( (tspan[1],0.5tspan[end]) ),
        parameters
    )

    solution = solve( problem
        , alg
        ;
        adaptive = false,
        save_everystep = save,
        dt = dt
        , tols...
    )

    revProblem = ODEProblem( _shapeEquations_01,
        eltype( parameters.p ).( endConditionsAV( parameters, -tspan[1] ) ),
        eltype( tspan ).( (tspan[end], 0.5tspan[end]) ),
        parameters
    )

    revSolution = solve( revProblem
        , alg
        ;
        adaptive = false,
        save_everystep = save,
        dt = -dt
        , tols...
    )
    return (
        u = [solution.u;reverse(revSolution.u)],
        t = [solution.t;reverse(revSolution.t)],
        prob = solution.prob
        )
end


"""
$(DocStringExtensions.SIGNATURES)
Integrate the shape equations from `tspan[1]` to `tspan[end]` for the parameters of `vesicle`.
"""
function ODEsolve( vesicle, tspan; plotkws=NamedTuple(), kwargs... )
    display( format.(vesicle.parameters, precision = 10) )
    solution, _ =  solve_shapeEquations( vesicle, tspan; kwargs... )
    return solution
end

"""
$(DocStringExtensions.SIGNATURES)
Integrate the shapeEquations using `solve_shapeEquations` save the difference to `endConditionsAV` in `res`.
"""
function root_residualAV!( res, parameters, vesicle, tspan; res_options = (force_symmetry = false, auto_m = false), kwargs... )
    # @show parameters, vesicle.parameters, force_symmetry
    if haskey(res_options, :force_symmetry) && (res_options.force_symmetry)
        vesicle.parameters .= parameters[[1,2,3,3,4]]
    elseif haskey( res_options, :auto_m ) && (res_options.auto_m)
        vesicle.parameters .= parameters[1:5]
        vesicle.spont_curvature = parameters[6]
    else
        vesicle.parameters .= parameters
    end # if
    solution, Δu = solve_shapeEquations( vesicle, (tspan[1],1tspan[end]);
            save_everystep = false,
            # callback = r_callback,
            callback = CallbackSet( r_callback, threepi_callback ),
            maxiters = 1e6,
            kwargs...
        )
    @debug begin
        output = ""
        if solution.retcode != :Success
            output *= "Returned with $(solution.retcode)\n"
        end # if
         output *= "Δu = $(Δu)"
         output
    end
    if solution.retcode == :Unstable
        error( "Instability detected at $(solution.prob.p)")
    end # if
    scale = sqrt( vesicle.area / 4π )
    if haskey(res_options, :force_symmetry) && (res_options.force_symmetry)
        res[1] = Δu[1]
        # res[1] = Δu[3] * scale
        res[2] = Δu[2] / scale
        res[3] = Δu[4] * scale # γ
        res[4] = abs(Δu[6]) / scale^3 + abs(Δu[5]) / scale^2 # V + A
        # res[3] = Δu[5] / scale^2 # A
        # res[4] = Δu[6] / scale^3 # V
        return nothing
    elseif haskey( res_options, :auto_m ) && (res_options.auto_m)
        res[1] = Δu[1]         # φ
        res[2] = Δu[2] / scale # r
        res[3] = Δu[3] * scale # u
        res[4] = Δu[4] * scale # γ
        res[5] = Δu[5] / scale^2 # A
        res[6] = Δu[6] / scale^3 # V
        return nothing
    else
        res[1] = Δu[2] / scale # r
        # res[2] = Δu[3] * scale # u
        res[2] = Δu[1]         # φ
        res[3] = Δu[4] * scale # γ
        res[4] = Δu[5] / scale^2 # A
        res[5] = Δu[6] / scale^3 # V
        return nothing
    end # if
end

"""
Finds the best `parameters` of the `vesicle` using root-finding.

$(DocStringExtensions.SIGNATURES)

## Positional arguments
 - `vesicle` : input vesicle (gets mutated)
 - `tspan`   : the interval of the arclength to integrate over, typically (0.0, 1.0)
 - `residual`: function that calculates the residual for the root-finding algorithm (`root_residualAV`)

## Keyword arguments
 - `solver`  : Currently supported solvers are `:nlsolve` and `:minpack` (`:nlsolve`)
 - additional keywords are passed to the solver routine
"""
ODEshoot_root!( vesicle, tspan, residual = root_residualAV!; solver::Symbol=:nlsolve, kwargs... ) = ODEshoot_root!(Val(solver), vesicle, tspan, residual; kwargs... )
"""
$(DocStringExtensions.SIGNATURES)
Alter the parameters using root-finding with `NLsolve.nlsolve`. Returns the object returned by `NLsolve.nlsolve`.
"""
function ODEshoot_root!( ::Val{:nlsolve}, vesicle, tspan, residual = root_residualAV!; res_options = ( force_symmetry = false, auto_m = false ), kwargs... )
    if haskey(res_options, :force_symmetry) && (res_options.force_symmetry)
        parameters = vesicle.parameters[[1,2,3,5]]
    elseif haskey( res_options, :auto_m ) && (res_options.auto_m)
        parameters = [vesicle.parameters; vesicle.spont_curvature]
    else
        parameters = vesicle.parameters
    end # if
    opt = nlsolve( (res,var) -> residual( res, var,  vesicle, tspan; res_options = res_options )
        , parameters
        ; iterations = 150
        , ftol = 5e-6
        , kwargs...
        , xtol = eps()
        # ,method = :anderson
        )
    if haskey(res_options, :force_symmetry) && (res_options.force_symmetry)
        vesicle.parameters .= opt.zero[[1,2,3,3,4]]
    elseif haskey( res_options, :auto_m ) && (res_options.auto_m)
        vesicle.parameters .= opt.zero[1:5]
        vesicle.spont_curvature = opt.zero[6]
    else
        vesicle.parameters .= opt.zero
    end # if
    return opt
end

function ODEshoot_root!( ::Val{:mdbm}, vesicle, tspan, residual = root_residualAV!; res_options = ( force_symmetry = false, ), points = 3, iterations = 10, box = 0.1, interpolationorder = 0, kwargs... )
    parameters = vesicle.parameters
    if res_options.force_symmetry
        parameters = vesicle.parameters[[1,2,3,5]]
    end # if
    cache = similar( parameters )
    p_range = range(parameters[1]*(1-box),(1+box)*parameters[1], length = points)
    Σ_range = range(parameters[2]*(1-box),(1+box)*parameters[2], length = points)
    u0_range = range(parameters[3]*(1-box),(1+box)*parameters[3], length = points)
    u1_range = range(parameters[4]*(1-box),(1+box)*parameters[4], length = points)
    S_range = range(parameters[5]*(1-box),(1+box)*parameters[5], length = points)
    @debug begin
        for p in Iterators.product( p_range, Σ_range, u0_range, u1_range, S_range )
            r = (p...)->(residual( cache, p, vesicle, tspan; res_options... ); cache)
            @show r(p...)
        end
        "Values of residuals"
    end # debug
    p_ax = Axis(p_range,"P")
    Σ_ax = Axis(Σ_range,"Σ")
    u0_ax = Axis(u0_range,"u0")
    u1_ax = Axis(u1_range,"u1")
    S_ax = Axis(S_range,"S")
    axes = [p_ax,Σ_ax,u0_ax,u1_ax,S_ax]
    problem = MDBM_Problem( (p...)->(residual( cache, p, vesicle, tspan; res_options... ); cache), axes )
    # TODO: crashes
    solutions = MDBM.solve!( problem, iterations, interpolationorder = interpolationorder, kwargs... )
    # TODO: figure out how this would work out
    # if res_options.force_symmetry
    #     vesicle.parameters .= opt.zero[[1,2,3,3,4]]
    # else
    #     vesicle.parameters .= opt.zero
    # end # if
    # return #solutions
end

function ODEshoot_root!( ::Val{:minpack}, vesicle, tspan, residual = root_residualAV!; res_options = ( force_symmetry = false, ), kwargs... )
    parameters = vesicle.parameters
    if res_options.force_symmetry
        parameters = vesicle.parameters[[1,2,3,5]]
    end # if
    opt = fsolve( (res,var) -> residual( res, var,  vesicle, tspan; res_options... )
        , parameters
        ; iterations = 200
        , tol = 1e-8
        , kwargs...
        )
    if res_options.force_symmetry
        vesicle.parameters .= opt.x[[1,2,3,3,4]]
    else
        vesicle.parameters .= opt.x
    end # if
    return opt
end
