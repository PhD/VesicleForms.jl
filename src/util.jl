using OrderedCollections
using Statistics
using ProgressMeter
using Setfield
using ArgCheck

"""
Changes the value of the variable of a `vesicle` given as `field` in steps of `dv` until `target` is reached.

$(DocStringExtensions.SIGNATURES)

## Positional arguments
 - `vesicle`: the vesicle to start with
 - `target` : the target value to reach
 - `tspan`  : the interval of the arclength to integrate over, typically (0.0, 1.0)
 - `field`  : valid symbol of a property of the vesicle to change (like `:v` (default) or `:m̄`)
 - `dv`     : stepsize (default: 1/100 of the distance to `target`)

## Keyword arguments
 - `silent` : whether to print the progress to stdout (false)
 - `stop`   : callback that can be applied to stop before reaching the `target` (nothing)
 - `store`  : container to `store` intermediate results that will be `push!`ed to it (nothing)
 - `residual` : function to compute the residual for the non-linear solver (`root_residualAV`)
 - `factor`  : initial factor for controlling the size of the trust-region
 - additional keyword arguments are passed to `ODEshoot_root`
 
Returns the final `Vesicle`.
"""
function change( vesicle, target, tspan, field::Symbol = :v, dv = 0.01(target - getproperty(vesicle, field));
    silent = false,
    stop = nothing,
    store = nothing,
    residual = root_residualAV!,
    factor = 1.0,
    kwargs... )
    @argcheck( field in propertynames(vesicle) || ( field in (:v,:m̄ ) ) )
    v_start = getproperty(vesicle, field)
    @argcheck( sign( target - v_start ) == sign(dv) )
    if v_start == target
        return nothing
    end

    v_range = round(v_start,sigdigits=8):dv:target
    silent || ( prog = ProgressUnknown( "Change $field: $v_range..." ) )
    op = (target > v_start) ? Base.:<= : Base.:>=
    v = v_start
    change! = v -> ODEshoot_root!( v, tspan, residual; factor = factor, kwargs... )
    change!(vesicle)
    v += dv
    while true
        old_vesicle = copy(vesicle)
        old_dv = copy(dv)
        setproperty!( vesicle, field, v )
        i = 0
        silent || ProgressMeter.next!( prog; showvalues = ( (field, getproperty(vesicle, field)), (:retry, i), (:factor,factor)) )
        old_factor = factor
        while !change!(vesicle).f_converged
            i += 1
            if i >= 10
                error( "Not converging")
                break
            end
            factor *= 0.75
            dv *= 0.5
            setproperty!( old_vesicle, field, getproperty(old_vesicle, field) + dv )
            silent || ProgressMeter.update!( prog, prog.counter; showvalues = ( (field, getproperty(vesicle, field)), (:retry, i), (:factor, factor)) )
        end
        !isnothing(store) && push!( store, copy(vesicle) )
        op(v, target) || break
        factor = old_factor
        dv = old_dv
        if !isnothing( stop )
            stop( vesicle ) && break
        end
        v += dv
    end
    silent || ProgressMeter.finish!( prog )
    return vesicle
end

function scan_m( vesicle0, ms, op = <;
    tspan = (0.0,1.0),
    n_restarts = 10,
    res_options = ( force_symmetry = false, dt = 1e-3, ),
    stop = nothing,
    kwargs...
    )
    vesicle01 = copy( vesicle0 )
    ms /= sqrt( vesicle0.area / 4pi )
    m = ms[1]
    dm = ms[2] - m
    old_m = vesicle01.spont_curvature
    if !haskey(kwargs.data, :factor)
        factor = 1.0
    end # if
    solutions = ODESolution[]
    progress = ProgressUnknown( "Do something with m..." )
    if op == <
        progress.desc = "Increasing m..."
    elseif op == >
        progress.desc = "Decreasing m..."
    end # if
    try
        while op(m, ms[end])
            vesicle01 = Vesicle( vesicle01, spont_curvature = m )
            restarts = 0
            old_parameters = copy(vesicle01.parameters)
            old_factor = copy(factor)
            old_dm = copy(dm)
            old_dt = res_options.dt
            ProgressMeter.next!( progress,
                showvalues = (
                    (:m,m),
                    (:dm,dm),
                    (:m_target,ms[end]),
                    (:dt, res_options.dt),
                    (:progress,string(round((m-ms[1])/ms[end]*100, sigdigits = 3))*"%"),
                    (:old_parameters, old_parameters),
                    (:factor, factor),
                    (:converged,"yes"),
                    (:restarts, restarts),
                    )
                )
            while !getproperty( VesicleForms.ODEshoot_root!( vesicle01, tspan; res_options = res_options, kwargs... ), (kwargs.data.solver == :minpack ? :converged : :f_converged) )
                if restarts > n_restarts
                    @error "All hope is lost. Exiting at m = $m..."
                    show( IOContext(stdout, :compact => false), vesicle01 )
                    solution = VesicleForms.solve_shapeEquations( vesicle01, tspan )[1]
                    push!( solutions, solution )
                    return solutions
                end # if
                restarts += 1
                factor *= 0.75
                dm *= 0.5
                # res_options = @set res_options.dt *= 0.75
                ProgressMeter.update!( progress, progress.counter,
                    showvalues = (
                        (:m,m),
                        (:dm,dm),
                        (:m_target,ms[end]),
                        (:dt, res_options.dt),
                        (:progress,string(round((m-ms[1])/ms[end]*100, sigdigits = 3))*"%"),
                        (:old_parameters, old_parameters),
                        (:factor, factor),
                        (:converged,"no"),
                        (:restarts, restarts),
                        (:EndVesicle, vesicle01)
                        )
                    )
                vesicle01.parameters .= old_parameters
                vesicle01 = Vesicle( vesicle01, spont_curvature = old_m + dm )
                # vesicle01.parameters[4] = vesicle01.parameters[3]
            end # while
            m += dm
            if res_options.dt < 1e-6
                res_options = @set res_options.dt = old_dt
            end # if
            factor = old_factor
            dm = old_dm
            old_m = vesicle01.spont_curvature
            solution = VesicleForms.solve_shapeEquations( vesicle01, tspan )[1]
            push!( solutions, solution )
            if !isnothing( stop )
                stop( solution ) && break
            end # if
        end
    catch e
        @show e
        rethrow(e)
    finally
        ProgressMeter.finish!(progress)
        return solutions
    end # try
end # function

"""
$(DocStringExtensions.SIGNATURES)
Adapt stepwise the reduced volume and adjust parameters via `ODEshoot_root!` until `target_v` is reached. Return the final `Vesicle`.
"""
function relax_to_v( vesicle, target_v, tspan, dv = 0.01(target_v - vesicle.red_volume);
    silent = false,
    residual = root_residualAV!,
    factor = 1.0,
    kwargs... )
    v_start = vesicle.red_volume
    if v_start == target_v
        return nothing
    end

    v_range = (v_start):dv:target_v
    prog = ProgressUnknown( "Relax to $target_v..." )
    oldVes = copy( vesicle )
    for v = v_range
        vesicle = VesicleForms.set_v!( vesicle, v )
        i = 0
        old_factor = factor
        while !ODEshoot_root!( vesicle, tspan, residual; factor = factor, kwargs... ).f_converged
            i += 1
            factor *= 0.75
            ProgressMeter.update!( prog, prog.counter; showvalues = ( (:v, vesicle.red_volume), (:retry, i), (:factor, factor)) )
            vesicle.parameters .= oldVes.parameters
            if i >= 10
                error( "Not converging")
                break
            end
        end
        factor = old_factor
        ProgressMeter.next!( prog; showvalues = ( (:v, vesicle.red_volume), (:retry, i), (:factor,factor)) )
        oldVes = copy( vesicle )
    end
    ProgressMeter.finish!( prog )
    silent || ODEsolve( vesicle, tspan )
    return vesicle
end


function bisect( f, u0₋, u0₊ )
    @assert f(u0₋).retcode == :R0
    @assert f(u0₊).retcode == :R0
    @assert f(u0₋).u[end][1] - pi < 0
    @assert f(u0₊).u[end][1] - pi > 0
    c = (u0₋ + u0₊) / 2
    fc = f(c)
    while fc.retcode == :R0 || fc.retcode == :SweetSpot
        if fc.retcode == :SweetSpot
            return c, fc
        elseif fc.u[end][1] - pi < 0
            u0₋ = c
        elseif fc.u[end][1] - pi > 0
            u0₊ = c
        end # if
        c = (u0₋ + u0₊) / 2
        if c == u0₋
            @warn "No sweet spot found at $(fc.prob.p)"
            return c, fc
        end # if
        fc = f(c)
    end # while
    @info "Bisection encountered $(fc.retcode). Excluding solution at $(fc.prob.p)"
    return missing
end # function

function solution_to_vesicle( sol )
    return Vesicle(
        sol.prob.p.m,
        sol.u[end][5],
        sol.u[end][6],
        convert(Vector, sol.prob.p.p),
        sol.prob.p.κ
    )
end # function

function bisect_to_vesicle( b )
    if ismissing(b)
        return missing
    end # if
    u0, sol = b
    sol.prob.p.p[3] = u0
    sol.prob.p.p[4] = sol.u[end][3]
    sol.prob.p.p[5] = sol.prob.p.p[5] * sol.t[end]
    return solution_to_vesicle( sol )
end # function

function findshapes( vesicle, P_range, Σ_range, u0_range, tspan; kwargs... )
    shapes = Vector{typeof(vesicle)}[]
    for P in P_range
        vesicle.parameters[1] = P
        append!( shapes,
            findshapes( vesicle, Σ_range, u0_range, tspan; kwargs... )
        )
    end # for
    return shapes
end # function

function findshapes( vesicle, Σ_range, u0_range, tspan; kwargs... )
    shapes = Union{Missing,Vector{Union{Missing,typeof(vesicle)}}}[]
    for Σ in Σ_range
        vesicle.parameters[2] = Σ
        push!( shapes, findshapes(
            vesicle, u0_range, tspan; kwargs...
        ) )
    end
    return collect.(skipmissing.( shapes |> skipmissing |> collect ) )
end # function

function findshapes( vesicle, u0_range, tspan; kwargs... )
    f = function (u)
            vesicle.parameters[3] = u
            parameters = collect( vesicle )
            problem = ODEProblem( shapeEquations,
                    initialConditionsAV( parameters, tspan[1] ),
                    tspan,
                    parameters
                )
            return solveProb( problem;
                callback = CallbackSet(
                    r_callback,
                    # threepi_callback
                    ),
                adaptive = true,
                kwargs...
             )
        end # function
    u0, u0s_rest = collect( u0_range )
    du0 = u0_range[2] - u0_range[1]
    sol = f(u0)
    ret = sol.retcode
    borders = Pair{eltype(u0_range),typeof(ret)}[]
    for u0 in u0s_rest
        oldu0 = vesicle.parameters[3]
        # @show vesicle.parameters
        oldret = ret
        sol = f(u0)
        ret = sol.retcode
        if oldret != ret #&&
            # oldret in (:Success,:R0) &&
            # ret in (:Success,:R0)
            push!( borders, oldu0 => oldret )
            push!( borders, u0 => ret )
        end # if
    end # for
    if isempty(borders); return missing; end
    if first( borders ).second == :R0
        u0 = first( borders ).first
        while (ret = f(u0).retcode) == :R0
            u0 -= du0
        end # while
        pushfirst!( borders, u0 => ret ,u0 + du0 => :R0 )
    end # if
    if last( borders ).second == :R0
        u0 = last( borders ).first
        while (ret = f(u0).retcode) == :R0
            u0 += du0
        end # while
        push!( borders, u0 - du0 => :R0, u0 => ret )
    end # if
    r0s = filter( x->x.second == :R0, borders )
    r0_intervalls = [ r0s[i].first => r0s[i+1].first for i in firstindex(r0s):2:lastindex(r0s) ]
    imp = map( int -> improve_intervall( f,int,borders, du0 ), r0_intervalls )
    imp = filter(!isequal(nothing),imp)
    g = function (u0s)
        u0₋ = ( f(u0s.first).u[end][1] - pi < 0 ? u0s.first : u0s.second )
        u0₊ = ( u0₋ == u0s.first ? u0s.second : u0s.first )
        bisect(f,u0₋,u0₊)
    end # function
    vs = bisect_to_vesicle.( map( g, imp ) )
    @info "Global relation of  vesicles: ($(relation_diff.(collect(skipmissing(vs)))))"
    return vs
end # function

function improve_intervall( f, intervall, borders, du; pred = (sol->sol.u[end][1] - pi) )
    i = intervall
    ff = pred(f(i.first))
    fs = pred(f(i.second))
    if ( ff < 0 && fs > 0 ) || ( ff > 0 && fs < 0 )
        return i
    elseif ff > 0 && fs > 0
        u_start, u_stop = find_success( i.first, borders )
        new_u = refine_intervall( f, u_start, u_stop, du, pred = pred )
        while u_stop == new_u
            du /= 2
            if du < 2e-5
                @warn "No sign change found for u ∈ range($(u_start),stop=$(u_stop),step=$(du)).\nIntervall will be removed."
                return nothing
            end # if
            new_u = refine_intervall( f, u_start, u_stop, du, pred = pred )
        end # while
        return new_u => i.second
    elseif ff < 0 && fs < 0
        u_start, u_stop = find_success( i.second, borders )
        new_u = refine_intervall( f, u_start, u_stop, du, comp = >, pred = pred )
        while u_stop == new_u
            du /= 2
            if du < 2e-5
                @warn "No sign change found for u ∈ range($(u_start),stop=$(u_stop),step=$(du)).\nIntervall will be removed."
                return nothing
            end # if
            new_u = refine_intervall( f, u_start, u_stop, du, pred = pred, comp = > )
        end # while
        return i.first => new_u
    else
        error("Improving intervall ($(i.first),$(i.second)) encounters: $ff,$fs")
    end # if
end # function

function find_success(u_start,borders)
    ind = findfirst(x->(x.first == u_start), borders)
    # @show borders, ind
    if borders[ind-1].second != :R0
        return borders[ind-1].first => borders[ind+2].first
    else
        return borders[ind-2].first => borders[ind+1].first
    end # if
end

function refine_intervall( f, u_start, u_stop, du; comp = <, pred = id )
        new_intervall = range(u_start, step = du/2, stop = u_stop)
        for u in new_intervall
            u == u_start && continue
            sol = f(u)
            ( comp(pred(sol), 0) && sol.retcode == :R0 ) && ( u_stop = u; break )
        end # for
        return u_stop
end # function

function scan( vesicle, u0_range; kwargs... )
    results = OrderedDict()
    for u0 in u0_range
        vesicle.parameters[3] = u0
        sol = ODEsolve( vesicle, (0.0,2.0); callback = VesicleForms.r_callback, adaptive = true, kwargs... )
        # @show sol.u[end][1]-pi, sol.u[end][2]
        sol.retcode == :SweetSpot && @info "Found a sweet spot!"
        results[u0] = ( sol.retcode, VesicleForms.relation_diff( sol ) )
    end # for
    return results
end # function

function belly_n_neck_value( solution::ODESolution; kwargs... )
    sol = solution
    parameters = sol.prob.p
    rs = sol[2,:]
    ds = sol.t[2] - sol.t[1]
    zs = parameters.p[5] * ds * cumsum( sin.( sol[1,:] ) )
    return belly_n_neck_value( rs; kwargs... )
end # function

function belly_n_neck_value( vecs; verbose = false )
    oldps = zeros(typeof(vecs[1]),10)
    function islocalext( p, comp )
        rightmean = Statistics.mean((p,oldps[1:4]...))
        leftmean = Statistics.mean(oldps[6:10])
        return comp(oldps[5],rightmean) && comp(oldps[5],leftmean)
    end # function
    nextmax = function(i)
    return findnext( vecs, i) do p
        pred = islocalext(p, >)
        for i in length(oldps):-1:2
            oldps[i] = oldps[i-1]
        end # for
        oldps[1] = p
        return pred
    end # do
    end # function
    nextmin = function(i)
    return findnext( vecs, i) do p
        pred = islocalext(p, <)
        for i in length(oldps):-1:2
            oldps[i] = oldps[i-1]
        end # for
        oldps[1] = p
        return pred
    end # do
    end # function
    ind_max1 = nextmax(1)
    ind_min = nextmin( ind_max1 )
    ind_max2 = nextmax( ind_min )
    verbose && @show ind_max1, ind_min, ind_max2
    return vecs[[ind_max1, ind_min, ind_max2]]
end # function
