using ColorSchemes
using Lazy
using Formatting
using LaTeXStrings
using RecipesBase

@recipe function vesicle_recipe( v::Vesicle{<:Real}, mode = :contour, tspan = (0.0,1.0); int = false, scaling = true )
    label --> latexstring("\\bar{m} = $(round(v.m̄, sigdigits = 4)),\\;" *
        "v = $(round(v.red_volume, sigdigits = 4))"
        )
    title --> ""
    linewidth --> 2
    legend --> :outertopright
    if int
        sol = integrate_shapeEquations( v, tspan )
    else
        sol, _ = solve_shapeEquations( v, tspan )
    end # if
    if sol.prob.p isa NamedTuple
        arc_length = sol.prob.p.p[5]
    else
        arc_length = sol.prob.p[5]
    end
    ts = range(sol.t[1],stop=sol.t[end],length=500)
    ds = sol.t[2] - sol.t[1]
    ϕ = sol[1,:]
    r_sol = sol[2,:]
    rz = r_sol
    z = arc_length * ds * cumsum( sin.(ϕ) )
    if scaling
        Rve = sqrt( sol.prob.p.A / 4pi )
        rz /= Rve
        z  /= Rve
    end # if
    if mode == :contour
        if isapprox(v.parameters[3],v.parameters[4], rtol = 9e-3)
            linecolor --> :blue
        else
            linecolor --> :red
        end
        size --> (180,260)
        aspect_ratio --> 1
        if scaling
            yguide --> latexstring("z / R_{ve}")
            xguide --> latexstring("r / R_{ve}")
        else
            yguide --> latexstring("z")
            xguide --> latexstring("r")
        end
        yl = maximum(z) - minimum(z)
        ylimits = (minimum(z)-0.1yl, maximum(z)+0.1yl)
        ylims --> ylimits
        @series begin
            seriestype := :path
            label := ""
            linewidth := 1
            linestyle := :dash
            linecolor := :gray
            ([0,0], collect(ylimits))
        end
        xlim = max( 1.1maximum( rz ), 0.7maximum(z) / 2 )
        xlims --> ( -xlim, xlim )
        xticks --> (-ceil(xlim):ceil(xlim), string.(abs.(-ceil(xlim):ceil(xlim))))
        z = [z; reverse(z)]
        rz = [rz; -reverse(rz)]
        (z, rz) |> reverse
    elseif mode == :mean_curvature
        xguide --> latexstring("s")
        yguide --> (scaling ? latexstring("M \\cdot R_{ve}") : latexstring("M"))
        xlims --> (0,1)
        u = sol[3,:]
        M = @. (u + sin(ϕ) / r_sol) / 2
        (sol.t[1:end-5], M[1:end-5])
    end
end # function

@recipe function undoloid_recipe( u::Undoloid; scaling = true, offset = 0.0 )
    size --> (200,260)
    title --> latexstring("\\bar{m} = $(round(u.m, sigdigits = 4))")
    label --> ""
    aspect_ratio --> 1
    linewidth --> 2
    z = u.z .- offset
    rz = u.r
    if scaling
        Rve = sqrt( u.area / 4pi )
        rz /= Rve
        z  /= Rve
        yguide --> latexstring("z / R_{ve}")
        xguide --> latexstring("r / R_{ve}")
    else
        yguide --> latexstring("z")
        xguide --> latexstring("r")
    end # if
    yl = maximum(z) - minimum(z)
    ylimits = (minimum(z)-0.1yl, maximum(z)+0.1yl)
    ylims --> ylimits
    @series begin
        seriestype := :path
        label := ""
        linewidth := 1
        linestyle := :dash
        linecolor := :gray
        ([0,0], collect(ylimits))
    end
    xlim = max( 1.1maximum( rz ), 0.7maximum(z) / 2 )
    xlims --> ( -xlim, xlim )
    xticks --> (-ceil(xlim):ceil(xlim), string.(abs.(-ceil(xlim):ceil(xlim))))
    z = [z; reverse(z)]
    rz = [rz; -reverse(rz)]
    (z, rz) |> reverse
end # function

function norm( t::Tuple{<:Number,<:Number} )
    return sqrt( sum( t.^2 ) )
end

# function plotSolution( solution::ODESolution; scale = false, verbose = true, kwargs... )
#     if solution.prob.p isa NamedTuple
#         arc_length = solution.prob.p.p[5]
#     else
#         arc_length = solution.prob.p[5]
#     end
#     ts = range(solution.t[1],stop=solution.t[end],length=500)
#     ds = solution.t[2] - solution.t[1]
#     ϕ = solution[1,:]
#     r_sol = solution[2,:]
#     rz = r_sol
#     z = arc_length * ds * cumsum( sin.(ϕ) )
#     if scale
#         Rve = sqrt( solution.prob.p.A / 4pi )
#         rz /= Rve
#         z  /= Rve
#     end # if
#     xlims = (-0.1, 1.1maximum(z))
#     ylims = (0, max(1.1maximum( rz ), maximum(z) ) )
#     radialPlot = plot( z, rz; lw = 2, xlabel = "Height", ylabel = "Radius", lab = "Contour", ylims = ylims, kwargs... )
#     if scale
#         xlabel!( radialPlot, latexstring("\\mbox{Height} / R_{ve}") )
#         ylabel!( radialPlot, L"\mbox{Radius} / R_{ve}" )
#         mbar = round( solution.prob.p.m * Rve, sigdigits = 4 )
#         title!( radialPlot, latexstring("\\mbox{\\noindent\\fbox{\$\\bar{m} = $mbar\$}}") )
#     end # if
#     if verbose
#         display( radialPlot )
#         area = solution.u[end][5]
#         volume = solution.u[end][6]
#         println( "Area: ", area )
#         println( "Volume: ", volume )
#         println( "red. Volume: ", reduced_volume( area, volume ) )
#         println( "Bend. Energy: ", maximum(solution[7,:]) )
#         println( "Aspect ratio: ", maximum(z) / 2maximum( rz ) )
#     end # if
#     return radialPlot
# end
#
# function plotSolution( solution::NamedTuple{( :u, :t, :prob )}; kwargs... )
#     if length( solution.prob.p ) > 5
#         z = [solution.u[i][8] for i = 1:length(solution.u)]
#         arc_length = solution.prob.p.p[5]
#     else
#         ts = solution.t
#         ds = ts[3] - ts[2]
#         arc_length = solution.prob.p.p[5]
#         z = arc_length * ds * cumsum( sin.([solution.u[i][1] for i = 1:length(solution.u)]) )
#     end
#     ebEst = (solution.u[end÷2][7]-solution.u[end÷2+1][7]) / (8pi)
#     rz = [solution.u[i][2] for i = 1:length(solution.u)]
#     # rz = vesicle.parameters[5] * cumsum( cos.(solution(ts,idxs=1)) )
#     radialPlot = Plots.plot( z, rz; lw = 2, xlabel = "Height / 10\\mu m", ylabel = "Radius / 10\\mu m", lab = "Contour", ylims = ( 0, max( maximum( rz ) , maximum( z ) / 2 ) ),
#         kwargs...,
#      )
#     display( radialPlot )
#     area = solution.u[end][5]
#     volume = solution.u[end][6]
#     println( "Area: ", area )
#     println( "Volume: ", volume )
#     println( "red. Volume: ", 3volume / (4pi * (area/(4pi))^(3/2) ) )
#     println( "Bend. Energy: ", ebEst, " ( estimate, scaled )" )
# end
