using Parameters
using StaticArrays
using LaTeXStrings
using OrdinaryDiffEq

"""
Basic data structure to hold all parameters that uniquely define a vesicle.

$(DocStringExtensions.TYPEDFIELDS)

"""
Base.@kwdef mutable struct Vesicle{ F <: Real }
    spont_curvature::F = 0.0
    area::F
    volume::F
    "these parameters are: osmotic pressure, surface tension, s-pole curvature, n-pole curvature, arc length"
    parameters::Vector{F}
    red_volume::F
    bend_rigidity::F = 8e-20
end

Base.show( io::IO, v::Vesicle ) = Base.show_default( IOContext(io, :compact => false), v )

function show_parameters( v::Vesicle; scaled = true )
    if scaled
        R = v.R
        P, Σ, u0, u1, S = round.(v.parameters .* [ R^3, R^2, R, R, 1/R/π ], sigdigits = 3)
        P = -P
        return "\$\\Delta p\$:~$P, \$\\sigma\$:~$Σ, \$\\bar{u}_0\$:~$u0, \$\\bar{u}_1\$:~$u1, \$\\bar{S}\$:~$S"
    else
        P, Σ, u0, u1, S = round.(v.parameters, sigdigits = 3)
        P = -P
        return latexstring("\\Delta P/\\kappa:~$P, \\Sigma/\\kappa:~$Σ, u_0:~$u0, u_1:~$u1, S:~$S")
    end
end # function

Vesicle{T}(v::Vesicle) where {T} = T === Rational ? Vesicle{T}((rationalize.(getproperty(v, p)) for p in propertynames(v))...) : Vesicle{T}((getproperty(v, p) for p in propertynames(v))...)
function Vesicle( v::Vesicle; kwargs... )
    nt = NamedTuple()
    for property in propertynames(v)
        if property in keys(kwargs)
            nt = merge( nt, NamedTuple{(property,)}(kwargs[property]) )
            continue
        end
        value = getproperty( v, property )
        if value isa Vector
            nt = merge( nt, NamedTuple{(property,)}(Ref(copy(value))) )
            continue
        end # if
        nt = merge( nt, NamedTuple{(property,)}(value) )
    end
    return Vesicle(;nt...)
end

function Vesicle( m::F, A::F, V::F, p::Vector{F}, k::F = 8e-20 ) where {F<:Real}
    v = Vesicle(; spont_curvature = m, area = A, volume = V, parameters = p, red_volume = reduced_volume(A,V), bend_rigidity = k )
    if length(p) == 4
        push!( p, F(1) )
        sol = integrate_shapeEquations( v, (0.0,Inf); adaptive = false, callback = r_callback )
        p[5] = sol.t[end]
    end # if
    return v
end # function

function Vesicle(sol::ODESolution)
    return Vesicle( sol.prob.p )
end # function

function Vesicle( nt::NamedTuple{(:p,:m,:A,:V,:κ)} )
    return Vesicle( ;parameters = Vector{eltype(nt.p)}(nt.p), spont_curvature = nt.m, area = nt.A, volume = nt.V, bend_rigidity = nt.κ, red_volume = reduced_volume( nt.A, nt.V ) )
end # function

function reduced_volume( A, V )
    return 3V / (4pi * (A/(4pi))^(3/2) )
end # function

function Vesicle( R1::F, R2::F ) where {F<:Real}
    m = (1/R1 + 1/R2) / 2
    return Vesicle( m, 4π * (R1^2 + R2^2), 4π/3 * (R1^3 + R2^3), [-4m / (R1 * R2), 2m^2, 1/R1, -1/R2, π * (R1 + R2)] )
end

Vesicle( array::AbstractArray{ <:Real,1} ) = Vesicle( array[6], array[7], array[8], array[1:5] )

Base.getproperty( v::Vesicle, s::Symbol ) = Base.getproperty( v, Val(s) )
function Base.getproperty( v::Vesicle, ::Val{T} ) where T
    V = typeof(v)
    if T == :P
        return getfield(v,:parameters)[1]
    elseif T == :Σ
        return getfield(v,:parameters)[2]
    elseif T == :U0
        return getfield(v,:parameters)[3]
    elseif T == :U1
        return getfield(v,:parameters)[4]
    elseif T == :S
        return getfield(v,:parameters)[5]
    elseif T == :v
        return getfield( v, :red_volume )
    elseif T == :m̄
        return v.spont_curvature * sqrt( v.area / 4pi )
    elseif T == :R
        return sqrt( v.area / 4pi )
    else
        return getfield(v,T)
    end # if
end # function

function bending_energy(v::Vesicle; scaled = true)
    sol, _ = solve_shapeEquations(v, (0.0,1.0))
    scaled ? maximum( sol[7,:] ) / 8pi : maximum( sol[7,:] )
end # function

function Base.:(==)( v1::Vesicle, v2::Vesicle )
        all( property->getproperty(v2, property) ≈ getproperty(v1, property), propertynames(v2) )
end # function
function Base.isapprox( v1::Vesicle, v2::Vesicle; kwargs... )
        all( property->isapprox(getproperty(v2, property), getproperty(v1, property); kwargs...), propertynames(v2) )
end # function

Base.setproperty!( v::Vesicle, s::Symbol, value ) = Base.setproperty!( v, Val(s), value )
Base.setproperty!( v::Vesicle, ::Val{:v}, value ) = set_v!( v, value )
Base.setproperty!( v::Vesicle, ::Val{:m̄}, value ) = Base.setproperty!( v, Val(:mbar), value )
Base.setproperty!( v::Vesicle, ::Val{:mbar}, value ) = Base.setfield!( v, :spont_curvature, value / v.R )
Base.setproperty!( v::Vesicle, ::Val{V}, value ) where {V} = Base.setfield!( v, V, value )
function setproperty( v::Vesicle, ::Val{V}, value ) where V
    Vesicle( v; V => value )
end # function

function set_v!( v::Vesicle{ <: Real }, value )
    oldVolume = v.volume
    volume = value / (6sqrt(pi)) * v.area^(3/2)
    parameters = copy( v.parameters )
    v.volume = volume
    v.red_volume = value
    return v
end

function copy( v::Vesicle{ <: Real } )
    return Vesicle( v.spont_curvature, v.area, v.volume, copy( v.parameters ), v.bend_rigidity )
end

function length( v::Vesicle{ <:Real } )
    return 4 + length( v.parameters )
end

function collect( v::Vesicle{ <: Real } )
    p = v.parameters
    return (
        p = SVector{length(p),eltype(p)}(p),
        m = v.spont_curvature,
        A = v.area,
        V = v.volume,
        κ = v.bend_rigidity
        )
end

function relation_diff( v::Vesicle{<:Real}, op = + )
    rel = op(3v.parameters[1] * v.volume, 2v.parameters[2] * v.area)
    return ifelse( v.spont_curvature == 0,
        rel,
        rel * √( v.area / pi ) / ( 4 * v.bend_rigidity * v.spont_curvature )
        )
end

function relation_diff( sol::ODESolution, op = + )
    rel = op(3sol.prob.p.p[1] * sol.u[end][6], 2sol.prob.p.p[2] * sol.u[end][5])
    return ifelse( sol.prob.p.m == 0,
        rel,
        rel * √( sol.u[end][5] / pi ) / ( 4 * sol.prob.p.κ * sol.prob.p.m )
        )
end

function quality( v::Vesicle; tspan = (0.0,1.0) )
    return maximum(solve_shapeEquations( v, tspan )[2][1:6])
end # function
