using DataFrames

function get_vesicles()
    vesicles_dict = Dict{Vesicle{Float64},Symbol}()
    for name in filter(x->isdefined(VesicleForms,x),names( VesicleForms, all = true ))
        if (vesicle = getfield( VesicleForms, name ) ) isa VesicleForms.Vesicle
            push!( vesicles_dict, copy(vesicle) => name )
        end # if
    end # for
    return vesicles_dict
end # function

"""
Don't move this function!
"""
function improve_prototypes(; overwrite = false )
    vesicles_dict = VesicleForms.get_vesicles()
    vesicles = [ dc.first for dc in vesicles_dict ]
    old_vesicles = deepcopy(vesicles)
    replaced_vesicles = Symbol[]
    file = @__FILE__
    filepath, fileextension = split(file,".")
    buf = IOBuffer()
    tspan = (0.0,1.0)
    comp = v -> maximum(solve_shapeEquations( v, tspan )[2][1:6])
    for (i, vesicle) in enumerate(vesicles)
        if  ODEshoot_root!( vesicle, (0.0,1.0), ftol = 1e-12).f_converged &&
            (old_vesicles[i] != vesicle) &&
            comp(old_vesilce[i]) > comp(vesicle)
            println( IOContext(buf, :compact=>false), "$(vesicles_dict[vesicle]) = ", vesicle )
            push!( replaced_vesicles, vesicles_dict[vesicle] )
        end # if
    end # for
    new_lines = split(String(take!(buf)),"\n")
    str = read(file, String)
    for (vesicle, line) in zip(replaced_vesicles, new_lines)
        str = replace(str, Regex("^"*string(vesicle)*".*\$","m") => line*" # improved")
    end # for
    if overwrite
        write(file, str)
    else
        write(filepath*"_improved."*fileextension, str)
    end # if
    return DataFrame([ (name = vesicles_dict[vesicle], diff = comp(vesicle) - comp(old_vesicle), new_value = comp(vesicle)) for (vesicle, old_vesicle) in zip( vesicles, old_vesicles ) ])
end # function

# sphere
function sphere(; volume = 1.0, m = 0.0, pressure = (r,m)->0 )
    v = volume # in picoliter
    reducedVolume = 1.0
    area = ( v / reducedVolume * 6sqrt( pi ) )^( 2 / 3 )
    radius0 = sqrt( area / (4pi) )
    p = pressure( radius0, m )
    sigma = (4m - p * radius0^2) / (2radius0) - 2m^2 # sigma => sphere

    return Vesicle(;
        spont_curvature = m,
        area = area,
        volume = v,
        parameters = [p, sigma, 1/radius0, 1/radius0, pi*radius0],
        red_volume = reducedVolume,
        )
end # function

noPressureSphere = Vesicle{Float64}(0.0, 4.835975862049408, 1.0, [9.072360965178126e-6, -2.8139657353572696e-6, 1.6119919549930708, 1.6119919554314712, 1.9488885448650741], 1.0000000000000002, 8.0e-20) # improved
critSphere = Vesicle{Float64}(0.0, 4.835975862049408, 1.0, [50.26141830018307, -15.589847758329647, 1.6119913127948087, 1.6119913134053532, 1.9488884478196213], 1.0000000000000002, 8.0e-20) # improved

# self-intersecting discoscythe
# aka prolate0_4277
# double_donut = Vesicle( 0.0, 4.835975862049408, 1.0, [4.40148, -1.36524, -1.08174, -1.08174, 4.64859, -0.415007] )
# prolate0_6 = Vesicle{Float64}(0.0, 6.790490455989313, 1.0, [78.1646, -17.2663, 3.08703, 3.08703, 3.78885], 0.60, 8.0e-20)
oblate0_9 = Vesicle{Float64}(0.0, 5.7331389332647635, 1.161729512240853, [44.33564636205173, -13.475871443847433, 0.060645886550416014, 0.06064600793866422, 1.9757767881241126], 0.9000000000000002, 8.0e-20)
prolate0_7 = Vesicle{Float64}(0.0, 6.13411760034899, 1.0, [56.86446996225631, -13.905293383327285, 2.7480644927632176, 2.7480646463995058, 3.1201464345176197], 0.7000000000000001, 8.0e-20)
left_pear = Vesicle{Float64}(0.635, 46.34821261890418, 21.713044786355262, [-0.9999999748543386, 0.716666639956104, 0.9965378866353697, 0.7230495162685538, 8.240584296941885], 0.7318084654066389, 8.0e-20) # improved
right_pear = Vesicle{Float64}(0.635, 46.348209461776236, 21.713042356767605, [-0.9999994598021126, 0.7166657262667855, 0.7230495125888234, 0.9965131465372532, 8.240584293451558], 0.7318084582942385, 8.0e-20) # improved
central_pear = Vesicle{Float64}(0.635, 41.38633625010995, 17.806919080747928, [-0.9999986464545486, 0.7166654792771009, 0.8832073001568816, 0.8831943035020153, 7.887000719974468], 0.7112618237288572, 8.0e-20) # improved
prolate0_685 = Vesicle{Float64}(0.0, 6.223343081339558, 1.0, [60.421371614707205, -14.563242964913172, 2.798017361713355, 2.798017530372013, 3.209152540136963], 0.6850000000000002, 8.0e-20)
prolate0_685m_max = Vesicle{Float64}(1.7034442502507863, 6.223343081339558, 1.0, [11.799876071714813, -2.0840211478183317, 2.309513816635781, 2.3097903047942494, 3.2630381079515067], 0.6850000000000002, 8.0e-20) # improved
prolate_sqrt2_m_max = Vesicle{Float64}(1.5318630031848677, 6.092947785379556, 1.0, [-10.64075018926374, 3.4784330488379243, 2.319867948785431, 2.3200533775941223, 3.0480427488314796], 0.7071067811865476, 8.0e-20) # improved
prolate_ideal_neck_m_max = Vesicle{Float64}(1.731479589259205, 6.092947785379556, 1.0, [-32.07641683594583, 8.244869172957323, 2.2199925442934205, 2.220225176710466, 3.0402649543131632], 0.7071067811865476, 8.0e-20) # improved
oblate0_835 = Vesicle{Float64}(0.0, 6.026911609642218, 1.161729512240853, [42.770599910023904, -12.366499973342224, -0.39673929695898696, -0.39673915239170393, 2.0068783641939256], 0.8350048069925894, 8.0e-20)
oblate0_65 = Vesicle{Float64}(0.0, 7.122162879784503, 1.161729512240853, [36.086226255708155, -8.829291341986698, -1.2831669326287505, -1.2831667549605412, 2.178057058414138], 0.6500000000000001, 8.0e-20)
oblate0_6 = Vesicle{Float64}(0.0, 7.512537260578801, 1.161729512240853, [33.893613674160726, -7.8619000327770365, -1.4531154359375438, -1.453115261078548, 2.2449237968227123], 0.6000000000000001, 8.0e-20)
mexicanHat = Vesicle{Float64}(0.0, 5.530942521651568, 0.5231516052997888, [93.78632969254673, -13.306358356202688, 5.230421260897471, 5.230406189127817, 2.909022580122271], 0.4277155415678585, 8.0e-20)
prolate_ideal_neck = Vesicle{Float64}(0.0, 5.187870398908626, 0.7856742013183861, [70.2249298961701, -15.95276428009083, 2.9563842273732193, 2.956384232274005, 2.8418249488433154], 0.7071067811865472, 8.0e-20)
prolate0_685 = Vesicle{Float64}(0.0, 6.223343081339558, 1.0, [60.42137184545974, -14.563243039309157, 2.798017250682829, 2.7980172557633787, 3.209152540238752], 0.6850000000000002, 8.0e-20)
prolate0_9 = Vesicle{Float64}(0.0, 4.835975862049408, 0.8999999999999998, [47.522919906213836, -13.266390012036117, 2.6280481717637163, 2.62804817342551, 2.2455737132183935], 0.8999999999999999, 8.0e-20)
prolate0_7 = Vesicle{Float64}(0.0, 4.835975862049408, 0.6999999999999998, [81.23495735078515, -17.637950991093362, 3.0950016966501055, 3.095001701866414, 2.770390505289179], 0.7, 8.0e-20)
