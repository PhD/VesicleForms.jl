using Test
using VesicleForms, OrdinaryDiffEq
using BenchmarkTools
using ForwardDiff
@static if isinteractive()
    using Pkg
    Pkg.add("MathLink")
    using MathLink
end
##
testicle = VesicleForms.left_pear
p = collect( testicle )
cache = zeros(7)
J = zeros(7,7)
@static if isinteractive()
    struct ML_InitialConditions
        expr
    end
    function ML_InitialConditions()
        W```
        expandBoundary[dgl_,ic_,order_]:=(
            s0 = ic[[1,1,1]];
            iv = dgl[[1,1,1]];
            tdgl = Series[dgl,{iv,s0,order},Assumptions->Equal@@@ic]//Normal;
            sol = Equal@@@SolveAlways[tdgl,{iv}][[-1]];
            dTable = Flatten@Table[D[ic[[i,1,0]][s],{s,j}],{i,Length[ic]},{j,order+1}]/.{s->s0};
            sol = Solve[sol,dTable,Reals][[1]]/.ic;
            sol
        );
        dgl = {
            \[Phi]'[t] == S1 *u[t],
            r'[t] == S1 *Cos[\[Phi][t]],
            u'[t] ==
            S1*(-u[t]/r[t] Cos[\[Phi][t]] + (Cos[\[Phi][t]] Sin[\[Phi][t]])/
            r[t]^2 + \[Gamma][t]/r[t] Sin[\[Phi][t]] +
            p r[t]/2 Cos[\[Phi][t]]),
            \[Gamma]'[t] ==
            S1*((u[t] - 2 m)^2/2 - Sin[\[Phi][t]]^2/(2 r[t]^2) +
            p r[t] Sin[\[Phi][t]] + \[CapitalSigma]),
            A'[t] == S1 2 \[Pi] r[t],
            V'[t] == S1 \[Pi] r[t]^2 Sin[\[Phi][t]],
            Eb'[t] == S1 \[Pi] r[t] (u[t] + Sin[\[Phi][t]]/r[t] - 2 m)^2
        };
        ic = {r[0] -> 0, \[Phi][0] -> 0, u[0] -> u0, \[Gamma][0] -> 0, A[0] -> 0, V[0] -> 0, Eb[0] -> 0};
        order = 3;
        derivatives0 = expandBoundary[dgl, ic, order];
        func = Series[{\[Phi][s], r[s], u[s], \[Gamma][s], A[s], V[s], Eb[s]}, {s, 0, order + 1}] /. derivatives0 /. ic // FullSimplify
        ``` |> weval
        return ML_InitialConditions(weval(W`Normal[func]`))
    end
    function (ML::ML_InitialConditions)(dt, p)
        W"Set"(W"sJ", dt) |> weval
        W"Set"(W"mJ", p.m) |> weval
        W"Set"(W"PJ", p.p[1]) |> weval
        W"Set"(W"SigmaJ", p.p[2]) |> weval
        W"Set"(W"u0J", p.p[3]) |> weval
        W"Set"(W"u1J", p.p[4]) |> weval
        W"Set"(W"SJ", p.p[5]) |> weval
        W"Set"(W"func", ML.expr) |> weval
        list = W```
        N[func /. {S1 -> SJ, u0 -> u0J,
        p -> PJ, \[CapitalSigma] -> SigmaJ, m -> mJ} /.
        s -> sJ /. {\[Phi]'''''[0]->0, r'''''[0]->0}]
        ``` |> weval
        return list.args
    end
    ML_initialConditions = ML_InitialConditions()
    reference0 = @show ML_initialConditions( 1e-3, p )
end
@static if isinteractive()
    struct ML_EndConditions
        expr
    end
    function ML_EndConditions(v::VesicleForms.Vesicle)
        ML_EndConditions(v.area, v.volume, VesicleForms.bending_energy(v, scaled = false))
    end
    function ML_EndConditions(A, V, Eb)
        W"Set"(W"AJ", A) |> weval
        W"Set"(W"VJ", V) |> weval
        W"Set"(W"EbJ", Eb) |> weval
        W```
        dgl = {
            \[Phi]'[t] == S1 *u[t],
            r'[t] == S1 *Cos[\[Phi][t]],
            u'[t] ==
            S1*(-u[t]/r[t] Cos[\[Phi][t]] + (Cos[\[Phi][t]] Sin[\[Phi][t]])/
            r[t]^2 + \[Gamma][t]/r[t] Sin[\[Phi][t]] +
            p r[t]/2 Cos[\[Phi][t]]),
            \[Gamma]'[t] ==
            S1*((u[t] - 2 m)^2/2 - Sin[\[Phi][t]]^2/(2 r[t]^2) +
            p r[t] Sin[\[Phi][t]] + \[CapitalSigma]),
            A'[t] == S1 2 \[Pi] r[t],
            V'[t] == S1 \[Pi] r[t]^2 Sin[\[Phi][t]],
            Eb'[t] == S1 \[Pi] r[t] (u[t] + Sin[\[Phi][t]]/r[t] - 2 m)^2
        };
        ic = {r[0] -> 0, \[Phi][0] -> \[Pi], u[0] -> u1, \[Gamma][0] -> 0, A[0] -> AJ, V[0] -> VJ, Eb[0] -> EbJ};
        order = 3;
        tdgl = ((Series[dgl, {t, 0, order}, Assumptions -> {r[0] == 0, \[Phi][0] == \[Pi], \[Gamma][0] == 0}] // Normal)) /. {Cos[\[Phi][0]] -> -1, Sin[\[Phi][0]] -> 0};
        sol = (Equal @@ # &) /@ SolveAlways[tdgl,{t}][[-1]];
        derivatives1 = derivatives1 = Solve[sol,
                {\[Gamma]'[0], \[Gamma]''[0], \[Gamma]'''[0], \[Gamma]''''[0],
                 \[Phi]'[0], \[Phi]''[0], \[Phi]'''[0], \[Phi]''''[0],
                 r'[0], r''[0], r'''[0], r''''[0],
                 u'[0], u''[0], u'''[0], u''''[0],
                 A'[0], A''[0], A'''[0], A''''[0],
                 V'[0], V''[0], V'''[0], V''''[0],
                 Eb'[0], Eb''[0], Eb'''[0], Eb''''[0],
             }, Reals][[1]];
        func = Series[{\[Phi][s], r[s], u[s], \[Gamma][s], A[s], V[s], Eb[s]}, {s, 0, order + 1}] /. derivatives1 /. ic // FullSimplify
        ``` |> weval
        return ML_EndConditions(weval(W`Normal[func]`))
    end
    function (ML::ML_EndConditions)(dt, p)
        W"Set"(W"sJ", dt) |> weval
        W"Set"(W"mJ", p.m) |> weval
        W"Set"(W"PJ", p.p[1]) |> weval
        W"Set"(W"SigmaJ", p.p[2]) |> weval
        W"Set"(W"u0J", p.p[3]) |> weval
        W"Set"(W"u1J", p.p[4]) |> weval
        W"Set"(W"SJ", p.p[5]) |> weval
        W"Set"(W"func", ML.expr) |> weval
        list = W```
        N[func /. {S1 -> SJ, u1 -> u1J,
        p -> PJ, \[CapitalSigma] -> SigmaJ, m -> mJ} /.
        s -> sJ /. {\[Phi]'''''[0]->0, r'''''[0]->0}]
        ``` |> weval
        return list.args
    end
    ML_endConditions = ML_EndConditions(testicle.area, testicle.volume, 0.0)
    reference1 = @show ML_endConditions( 1e-3, p )
end
##

@testset "Boundary conditions" begin
    dt = 1e-3
    reference0 = [0.008212037435768182, 0.008240491675743122, 0.9965316889710862, 0.002121886326118216, 0.00021333565458095142, 3.6092396623781074e-9, 5.576941733732582e-5] # numbers from SeifertDGL.nb
    reference1 = [3.1475509996958775, -0.00824053553748624, 0.7230479204146236, 0.00498413452618291, 46.34799928268183, 21.713044783736535, -3.3078308806182376e-6] # numbers from SeifertDGL.nb
    @test VesicleForms.initialConditions( 0.0, p) ≈ [0.0,0.0,p.p[3],0.0]
    @test VesicleForms.initialConditions( dt, p)[1] ≈ reference0[1] rtol = 1e-12
    @test VesicleForms.initialConditions( dt, p)[2] ≈ reference0[2] rtol = 1e-12
    @test VesicleForms.initialConditions( dt, p)[3] ≈ reference0[3] rtol = 1e-11 # ??
    @test VesicleForms.initialConditions( dt, p)[4] ≈ reference0[4] rtol = 1e-12
    @test VesicleForms.initialConditionsAV( p) ≈ [0.0,0.0,p.p[3],0.0,0.0,0.0,0.0]
    @test VesicleForms.initialConditionsAV( p, dt)[5] ≈ reference0[5] rtol = 1e-12
    @test VesicleForms.initialConditionsAV( p, dt)[6] ≈ reference0[6] rtol = 1e-12
    @test VesicleForms.initialConditionsAV( p, dt)[7] ≈ reference0[7] rtol = 1e-12
    @test VesicleForms.rightBoundary( 0.0, p) ≈ [pi,0.0,p.p[4],0.0]
    @test VesicleForms.rightBoundary( dt, p) ≈ reference1[1:4] rtol = 1e-12
    @test VesicleForms.endConditionsAV( p) == [pi,0.0,p.p[4],0.0,testicle.area,testicle.volume, 0.0]
    @test VesicleForms.endConditionsAV( p, dt )[5] ≈ reference1[5] rtol = 1e-12
    @test VesicleForms.endConditionsAV( p, dt )[6] ≈ reference1[6] rtol = 1e-12
    @test VesicleForms.endConditionsAV( p, dt )[7] ≈ reference1[7]
end
# those are broken because of the modification for small r
@testset "Shape equations" begin
    it = 1e-3
    ic = VesicleForms.initialConditionsAV( p, it )
    ic0 = VesicleForms.initialConditionsAV( p )
    state = [pi/4,2.0,1.3,0.5]
    reference = [10.712754,5.826969998910240,-7.127685498474336,-6.259516239960477,4pi*8.240580000000000,4/3 * pi*17.480909996730716,8pi*0.303074530491827] # fromJaime/shapeeq.m
    tspan = (0.0,1.0)
    @testset "Single step" begin
        VesicleForms._shapeEquations( cache, state, p )
        @test_broken cache ≈ reference # reference needs to be recalculated
        VesicleForms._shapeEquations_jac( J, ic, p, it )
        @test J ≈ ForwardDiff.jacobian( (y,x)->(VesicleForms._shapeEquations(y,x,p, it)), cache, ic )
        VesicleForms._shapeEquations_jac( J, ic0, p, 0.0 )
        @test_broken J ≈ ForwardDiff.jacobian( (y,x)->VesicleForms._shapeEquations(y,x,p,0.0), cache, ic0 )
    end # testset
    @testset "Known form" begin
        sol = solve( VesicleForms.shapeProblem, Vern7(), save_everystep = false, adaptive = false, dt = 1e-3 )
        @test sol[1,end] ≈ pi rtol = 1e-5
        @test sol[2,end] ≈ 0 atol = 1e-6
        @test sol[3,end] ≈ VesicleForms.shapeProblem.p.p[4] rtol = 1e-3
        @test sol[4,end] ≈ 0 atol = 1e-4
        @test sol[5,end] ≈ VesicleForms.shapeProblem.p.A rtol = 1e-6
        @test sol[6,end] ≈ VesicleForms.shapeProblem.p.V rtol = 1e-6
        @test sol[7,end] ≈ 8pi rtol = 1e-6
    end # testset
end

@testset "shooting_methods" begin
    it = dt = 1e-3
    vesicle = copy( VesicleForms.critSphere )
    p = collect( vesicle )
    ic = VesicleForms.initialConditionsAV( p, it )
    ic0 = VesicleForms.initialConditionsAV( p )
    tspan = (0.0,1.0)
    @testset "matchShot" begin
        ret = VesicleForms.matchShot( vesicle, tspan, dt = dt, alg = Vern9() )
        @test length( ret.u ) == ( ( tspan[end] - tspan[1] ) / dt ) + 2
        ret = VesicleForms.matchShot( vesicle, tspan, false, dt = dt, alg = Vern9() )
        @test length( ret.u ) == 4
        r = ret.u[2][2]
        @test ret.u[4][5] ≈ 4pi * r^2
        @test ret.u[4][6] ≈ 4pi/3 * r^3 rtol = 1e-7
        @test ret.u[2][7] - ret.u[3][7] ≈ 8pi rtol = 1e-8
    end # testset
    @testset "ODEsolve" begin
        tend = 0.999
        @testset "Integrate 0 - tend < 1" begin
            ret = VesicleForms.integrate_shapeEquations( vesicle, (tspan[1],tend), dt = dt, alg = Vern7() )
            @test ret.retcode == :Success
            @test ret[3,1] == VesicleForms.critSphere.parameters[3]
            @test ret[3,end] ≈ VesicleForms.critSphere.parameters[4] rtol = 1e-3
            @test ret[3,end] ≈ endConditionsAV( collect(vesicle), tend - 1 )[3] rtol = 1e-3
            diff = ret[end] - endConditionsAV( collect(vesicle), tend - 1 )
            @test diff[1] ≈ 0 atol = 2e-6
            @test diff[2] ≈ 0 atol = 1e-7
            @test diff[3] ≈ 0 atol = 1e-3
            @test diff[4] ≈ 0 atol = 5e-6
            @test diff[5] ≈ 0 atol = 1e-4
            @test diff[6] ≈ 0 atol = 2e-8
            @test diff[7] ≈ 8pi rtol = 1e-4
        end # testset
        @testset "Integrate 0 - 1" begin
            ret = VesicleForms.integrate_shapeEquations( vesicle, (0.0,1.0), dt = dt, callback = nothing, alg = Vern9() )
            @test ret[3,1] == VesicleForms.critSphere.parameters[3]
            @test_broken ret[3,end] ≈ VesicleForms.critSphere.parameters[4] rtol = 1e-3
            @test ret.retcode == :Success
            diff = ret[end] - endConditionsAV( collect(vesicle) )
            @test_broken diff[1] ≈ 0 atol = 1e-6
            @test diff[2] ≈ 0 atol = 1e-5
            @test_broken diff[3] ≈ 0 atol = 1e-3
            @test_broken diff[4] ≈ 0 atol = 1e-6
            @test diff[5] ≈ 0 atol = 1e-7
            @test diff[6] ≈ 0 atol = 1e-7
            @test diff[7] != 0
        end # testset
        @testset "Terminate by callback" begin
            cb = :(VesicleForms.r_callback)
            global dret = ret = VesicleForms.integrate_shapeEquations( vesicle, (0.0,2.0), dt = dt, callback = eval(cb), alg = Vern7(), adaptive = true )
            @test_broken ret.retcode == :SweetSpot
            @info "Endpoint of integration with `$(cb)`: $(ret.t[end])"
            diff = ret[end] - endConditionsAV( collect(vesicle), ret.t[end] - 1 )
            @test diff[1] ≈ 0 atol = 1e-4
            @test diff[2] ≈ 0 atol = 1e-8
            @test_broken diff[3] ≈ 0 atol = 3e-4
            @test_broken diff[4] ≈ 0 atol = 3e-7
            @test diff[5] ≈ 0 atol = 1e-7
            @test diff[6] ≈ 0 atol = 1e-7
            @test_broken diff[7] ≈ 0 atol = 1e-6
            @test ret[3,1] == VesicleForms.critSphere.parameters[3]
            @test_broken ret[3,end] ≈ VesicleForms.critSphere.parameters[4] rtol = 1e-3
        end # testset
    end # testset
end # testset
##
if isinteractive()
    Pkg.rm("MathLink")
end
