using VesicleForms
using MDBM
using Test

vesicle = copy( VesicleForms.prolate0_9 )
vesicle.parameters[3] /= 2
tspan = (0.0,1.0)
VesicleForms.solve_shapeEquations( vesicle, tspan )
vesicle.parameters[3] = VesicleForms.prolate0_9.parameters[3]*2
VesicleForms.solve_shapeEquations( vesicle, tspan )
##
sol = VesicleForms.ODEshoot_root!( vesicle, tspan, solver = :mdbm, points = 5, interpolationorder = 0 )
getinterpolatedsolution( sol )
# @testset "Relaxing" begin
# relax0_7 = relax_to_v( copy(vesicle), 0.7, tspan, -0.01, solver = :mdbm, silent = true )
#     for property in propertynames(relax0_7)
#         @test getproperty(relax0_7, property) ≈ getproperty(VesicleForms.prolate0_7, property)
#     end # for
# end # testset
