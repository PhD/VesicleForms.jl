using VesicleForms
using OrdinaryDiffEq
using Test

vesicles_dict = VesicleForms.get_vesicles()
vesicles = [ dc.first for dc in vesicles_dict ]
vesiclesm0 = filter( v->(v.spont_curvature == 0), vesicles)
no_spheres = filter(v->(vesicles_dict[v] ∉ (:noPressureSphere,:critSphere)), vesicles)
@testset "Global shape equation" begin
    for vesicle in vesiclesm0
        reldiff = VesicleForms.relation_diff( vesicle )
        isfulfilled = isapprox( reldiff, 0, atol=5e-7)
        !isfulfilled && @info "It failed for $(vesicles_dict[vesicle]). Which gave $reldiff."
        @test isfulfilled #
    end # for
end # testset
##
function test_sweetness( sol, vesicle )
    ret = ( sol.retcode == :SweetSpot )
    if ret
        @info "Test for $(vesicles_dict[vesicle]) returned \e[32m$(sol.retcode)\e[39m"
    else
        @info "Test for $(vesicles_dict[vesicle]) returned \e[31m$(sol.retcode)\e[39m";
    end # if
    return ret
end # function

@testset "Sweetness" begin
    sweetness = 0
    for vesicle in vesicles
        sol = VesicleForms.integrate_shapeEquations( vesicle, (0.0,1.2), callback = VesicleForms.pi_callback, adaptive = true, alg = Vern7() )
        test_sweetness( sol, vesicle ) && ( sweetness += 1 )
    end # for
    @info "Sweetness: $sweetness /  $(length(vesicles))."
end # testset

@testset "Convergence" begin
    tspan = (0.0,1.0)
    for vesicle in no_spheres
        isconverged = ODEshoot_root!( vesicle, tspan, ftol = 1e-11 ).f_converged
        !isconverged && @info "$(vesicles_dict[vesicle]) did not converge."
        @test isconverged
        # its hard to get this and the global relationship
        # quality = VesicleForms.quality(getproperty(VesicleForms, vesicles_dict[vesicle]))
        # iswellstored =  quality < 3e-4
        # !iswellstored && @info "$(vesicles_dict[vesicle]) is not well stored. (Quality: $quality)"
        # @test_broken iswellstored
    end
end # testset
##
