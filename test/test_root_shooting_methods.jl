using VesicleForms
using Test

vesicle = copy( VesicleForms.prolate0_9 )
tspan = (0.0,1.0)

##
@testset "Relaxing" begin
    relax0_7 = relax_to_v( copy(vesicle), 0.7, tspan, -0.01, ftol = 1e-12, silent = true )
    # @show VesicleForms.quality.((relax0_7,VesicleForms.prolate0_7))
    @test relax0_7 ≈ VesicleForms.prolate0_7 rtol = 1e-3
end # testset
