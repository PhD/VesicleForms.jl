using VesicleForms
using OrdinaryDiffEq
using Test

#TODO: needs complete rework:
#     @testset "bisect" begin
#         vesicle = VesicleForms.prolate0_9 # former prolate0_9
#         tspan = ( 0.0, 2.0 )
#         kwargs = (
#         callback = VesicleForms.r_callback, adaptive = true, dt = 1e-3
#     )
#     # u0, sol = VesicleForms.bisect( 0.076, 0.08 ) do u0
#     u0, sol = VesicleForms.bisect( 0.999vesicle.parameters[3], 1.001vesicle.parameters[3] ) do u0
#         vesicle.parameters[3] = u0
#         # shapeEquations = VesicleForms.shapeEquations
#         shapeEquations = ODEFunction( VesicleForms._shapeEquations
#                                         ; jac = VesicleForms._shapeEquations_jac
#                 )
#         problem = ODEProblem( shapeEquations,
#             VesicleForms.initialConditionsAV( collect( vesicle ), tspan[1] ),
#             tspan,
#             collect( vesicle )
#             )
#         solution = VesicleForms.solveProb( problem; alg = Vern9(), kwargs... )
#         # @show solution.retcode
#         return solution
#     end # do
#     # @show u0
#     @test VesicleForms.relation_diff( sol ) ≈ 0 atol = 2e-8
#     @test sol.retcode == :SweetSpot
#     @test sol.u[end][1] ≈ pi rtol = 1e-6
#     @test sol.u[end][2] ≈ 0 atol = 2e-16
# end # testset
#
@testset "find_success" begin
    borders = [ 0.0 => :Success, 1.0 => :R0, 2.0 => :R0, 3.0 => :Success ]
    @test VesicleForms.find_success( 1.0, borders ) == (0.0 => 3.0)
    @test VesicleForms.find_success( 2.0, borders ) == (0.0 => 3.0)
end # testset

@testset "refine_intervall" begin
    @test VesicleForms.refine_intervall( 0.0, 1.0, 0.5; pred = (sol->-1) ) do u
        return ( retcode = :R0, )
    end #= do =# == 0.25
    @test VesicleForms.refine_intervall( 0.0, 1.0, 0.5; comp = >, pred = (sol->-1) ) do u
        return ( retcode = :R0, )
    end #= do =# == 1.0
end # testset

if Test.get_testset_depth() != 1 # sandbox guard
    starticle = VesicleForms.prolate0_9 |> copy
    u0 = starticle.parameters[3]
    P = starticle.parameters[1]
    u0s = range( -2u0 * P^(-1/3), stop = 2u0 * P^(-1/3), length = 100 )
    (res = VesicleForms.findshapes( starticle, u0s )) .|> VesicleForms.plotVesicle
    show( VesicleForms.relation_diff.(res) )
end # if
