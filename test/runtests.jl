using Test
using TimerOutputs
using Serialization
function runtests( to )
    @timeit to "Shape Equations" @testset "Shape Equations" begin include("shapeEquations_test.jl") end
    @timeit to "Shooting Methods" @testset "Shooting Methods" begin
        include("test_root_shooting_methods.jl")
        # include("test_MDBM.jl")
    end
    @timeit to "Vesicles" @testset "Vesicles" begin include("Vesicle_test.jl") end
    @timeit to "Utilities" @testset "Utilities" begin include("util_tests.jl") end
    @timeit to "Vesicle Prototypes" @testset "Vesicle Prototypes" begin include("vesicle_prototypes_test.jl") end
end # function
timings_file = joinpath(@__DIR__, "timings.jls")
const to = TimerOutput()
@timeit to "Include compilation time" runtests(to)
const to_c = TimerOutput()
@timeit to_c "Without compilation time" runtests(to_c)
print_timer(to, title = "Timings of VesicleForms", compact = true)
println()
print_timer(to_c, title = "Timings of VesicleForms", compact = true)
println()
try
    old_to, old_to_c = deserialize(timings_file)
    print_timer(old_to, title = "Former timings", compact = true)
    println()
    print_timer(old_to_c, title = "Former timings", compact = true)
    println()
catch err
    @info "Couldn't deserialize old timings.\nRecieved: $err."
end
serialize(timings_file, (to, to_c))
