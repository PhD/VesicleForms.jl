[![pipeline status](https://gitlab.gwdg.de/PhD/VesiclesForms.jl/badges/master/pipeline.svg)](https://gitlab.gwdg.de/PhD/VesiclesForms.jl/commits/master)
[![coverage report](https://gitlab.gwdg.de/PhD/VesiclesForms.jl/badges/master/coverage.svg)](https://gitlab.gwdg.de/PhD/VesiclesForms.jl/commits/master)
[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)](https://phd.pages.gwdg.de/VesicleForms.jl)
# VesicleForms.jl

Shapes of  axisymmetric `Vesicle`s can be computed by solving the corresponding shape equations using `DifferentialEquations.jl`.

## Installation

- Download the latests stable release of julia from https://www.julialang.org
- Start the julia executable to enter the REPL
- Add this package via:

```julia
using Pkg
Pkg.add("https://gitlab.gwdg.de/PhD/VesiclesForms.jl/")
```

## Usage

Solving the shape equations for a known solution can be done via `ODEsolve`:
```julia
using Plots, VesicleForms
vesicle = VesicleForms.prolate0_7 # the package comes with a bunch of vesicle prototypes
tspan = (0.0,1.0) # The arc-length is scaled to be in [0,1]
sol = ODEsolve(vesicle, tspan) # return a ODESolution
plot(sol)
```

For finding new solutions the shooting method can be used via `ODEshoot_root!` powered by `NLsolve.jl`:
```julia
vesicle = copy(VesicleForms.prolate0_7) # copy, since `ODEshoot_root!` is mutating
vesicle.v = 0.695
tspan = (0.0,1.0)
ODEshoot_root!(vesicle, (0.0,1.0), ftol = 1e-12)
plot(vesicle)
```

Further examples can be found in the `scripts` folder.

## Visualization

This package provides a `Vesicle` type with defined plot recipes for convenient visualization via `Plots.jl`:

```julia
using Plots, VesicleForms
vesicle = VesicleForms.sphere()
plot(vesicle) # displays the contour of the vesicle by default
```
