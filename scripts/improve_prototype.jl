using VesicleForms

printlong(x...) = printlong( stdout::IO, x... )
printlong(io::IO, x... ) = println(IOContext(io, :compact=>false), x...)
name = :oblate0_9
function improve( name; write = false, verbose = false )
    vesicle = copy(getproperty(VesicleForms, name))
    oldVesicle = copy(vesicle)
    relation_diff = VesicleForms.relation_diff
    verbose && vesicle |> printlong
    verbose && println( VesicleForms.relation_diff(vesicle) )
    verbose && VesicleForms.plotVesicle( vesicle, verbose = false ) |> display
    verbose && VesicleForms.plotVesicle( vesicle, int = true, verbose = false )
    ## improve
    ODEshoot_root!( vesicle, (0.0,1.0), ftol = 1e-12 ).f_converged || @warn "Did not converge for $name !"
    improvement = relation_diff(oldVesicle) / relation_diff(vesicle)
    @info "relation_diff($name) improved by $(improvement)\n"*
        "New value is $(relation_diff(vesicle))"
    verbose && printlong(vesicle)
    verbose && println( VesicleForms.relation_diff(vesicle) )
    verbose && VesicleForms.plotVesicle( vesicle, int = true, verbose = false ) |> display
    verbose && VesicleForms.plotVesicle( vesicle, verbose = false )
    ## append to file
    if write && ( abs(improvement) > 1.0)
        open( "src/vesicle_prototypes.jl", "a" ) do io
            printlong( io , string(name) * " = ", vesicle )
        end # open
    end # if
end # function

## improve all
vesicles_dict = VesicleForms.get_vesicles()
vesicles = [ dc.first for dc in vesicles_dict ]
vesiclesm0 = filter( v->(v.spont_curvature == 0), vesicles)
for vesicle in vesiclesm0
    improve(vesicles_dict[vesicle], verbose = false, write = false)
end # for
