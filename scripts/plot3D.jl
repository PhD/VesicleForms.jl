using Makie
using LaTeXStrings

function plotSolution3D( shape, colormap = :Spectral )
   scene = Scene(resolution = (700, 700))
   Φ = 0f0:2pi/20:2pi
    z = Float32[shape.z[i] for i = 1:length(shape.z), j in 1:length(Φ)]

    φ = Float32[Φ[j] for i = 1:length(shape.z), j in 1:length(Φ)]

    R = Float32[shape.r[i] for i = 1:length(shape.r), j in 1:length(Φ)]

    x = R .* cos.(φ)

    y = R .* sin.(φ)

    Makie.surface!(scene, x, y, z, colormap = colormap, shading = false )
    axis = scene[Makie.Axis]
    axis[:names][:axisnames] = (
      "Radius / R_{ve}",
      "Radius / R_{ve}",
      "Height / R_{ve}",
      )
    axis[:names][:textsize] = 10
    axis[:names][:rotation] = Float32.((pi,pi/2,pi/2))
    axis[:names][:gap] = 10
    scene
    # Makie.scatter(vec(x), vec(y), vec(z))
   # center!( scene )
end

##
using Plots
plotly()

function plotSolution3D( shape; colormap = :viridis, kwargs... )
   Φ = 0f0:2pi/20:2pi
    z = Float32[shape.z[i] for i = 1:length(shape.z), j in 1:length(Φ)]

    φ = Float32[Φ[j] for i = 1:length(shape.z), j in 1:length(Φ)]

    R = Float32[shape.r[i] for i = 1:length(shape.r), j in 1:length(Φ)]

    x = R .* cos.(φ)

    y = R .* sin.(φ)

    this_plot = surface( x, y, z; cbar = false, kwargs... )
end
