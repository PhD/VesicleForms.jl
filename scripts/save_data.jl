using FileIO
using VesicleForms
resicle
filename = joinpath(@__DIR__,"../simulation_data/increase_m.jld2")
save( filename, "ideal_neck_0-max", VesicleForms.solution_to_vesicle.(resicle) )
# save( joinpath(@__DIR__,"../simulation_data/increase_m.jld2"), "ideal_neck_0-max", vesicles )
##
using FileIO
using VesicleForms
vesicles = load( "simulation_data/increase_m.jld2" )["ideal_neck_0-max"]

## append new data set

using FileIO
using JLD2
using VesicleForms
filename = joinpath(@__DIR__,"../simulation_data/increase_m.jld2")
jldopen( filename, "a+" ) do file
    # file["asymm_Rne-0_15-0"] = needed_ves
    file["v=0_685-m=0-2"] = VesicleForms.solution_to_vesicle.(resicle)
end # do
