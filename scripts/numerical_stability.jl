using VesicleForms, Plots;
pgfplotsx();
function _shapeEquations(newState, state, parameters, time = nothing; rcut = 1e-1)
    pressure, Σ, v0, v1, S = parameters.p
    φ, r, v, γ = state
    m = parameters.m
    sinPhi, cosPhi = sincos(φ)
    sin2Phi = sinPhi * sinPhi
    # end
    newState[1] = S * v
    newState[2] = S * cosPhi
    newState[3] =
        S * (
            cosPhi * sinPhi / r^2 + γ * sinPhi / r - v * cosPhi / r +
            pressure * r * cosPhi / 2
        ) # u = \dot{φ}
    newState[4] =
        S * (((v - 2m)^2 - (sin2Phi / r^2)) / 2 + pressure * r * sinPhi + Σ) # γ
    newState[5] = S * 2pi * r # A
    newState[6] = S * pi * sinPhi * r^2 # V
    newState[7] = S * pi * r * (v + sinPhi / r - 2m)^2 # bending Energy
    return nothing
end
##
state(r) = [π, r, 1, 0, 4pi, 4pi / 3, 8pi]
parameters = (m = 0, p = VesicleForms.sphere(volume = 4pi / 3).parameters)
rs = [1 / 10^x for x = 1:16]
function f(r)
    cache = zeros(7)
    _shapeEquations(cache, state(r), parameters)
    return cache
end
fs = [f(r)[3] for r in rs]
ps = [
    plot(
        rs,
        [f(r)[i] for r in rs],
        xflip = true,
        xscale = :log10,
        xlims = (1e-12, 0.1),
        ylims = (-1, 1),
    ) for i = 3:4
]
display.(ps)
# plot!(rs, [f(r)[1] for r in rs])
# plot!(rs, [f(r)[2] for r in rs])
