using FileIO
using VesicleForms
using OrdinaryDiffEq

loadpath = joinpath(@__DIR__,"../simulation_data/")
vesicles = load( loadpath*"increase_m.jld2", "ideal_neck_0-max" )
undolicles = load( loadpath*"increase_m.jld2", "undoloids_max-sqrt2" )
m_ind = findfirst( x->(println(x);x<=0.15),
    getindex.( VesicleForms.belly_n_neck_value.(
     getindex.( VesicleForms.solve_shapeEquations.( vesicles, Ref((0.0,1.0)) ), 1 ) ), 2 ) ./ (sqrt.(getproperty.( vesicles, :area ) ./ 4pi))
    )
needed_ves = vesicles[m_ind:end]
## Decrease m

vesicle = first( needed_ves )
dm = 0.001
ms = vesicle.m̄:-dm:0
resicles = VesicleForms.scan_m( vesicle, ms, >;
        gif = false,
        solver = :nlsolve,
        ftol = 1e-12,
        # stop = function( sol )
        #         VesicleForms.belly_n_neck_value( sol )[2] / vesicle.R >= 0.15
        # end # function
    )

## Plot shapes

prepend!( needed_ves, reverse( VesicleForms.solution_to_vesicle.(resicles) ) )
plotVesicle.( needed_ves )
