using VesicleForms
using OrdinaryDiffEq, DiffEqCallbacks
using OrderedCollections

##

starticle = VesicleForms.prolate0_9 |> copy
u0 = starticle.parameters[3]
P = starticle.parameters[1]
u0s = range( -u0 * P^(-1/3), stop = 2u0 * P^(-1/3), length = 100 )
d = VesicleForms.scan( starticle, u0s ) |> show
vs = VesicleForms.findshapes( starticle, u0s, (0.0,1.0) )
VesicleForms.plotVesicle.( vs )
##
d = scan( starticle, range( 0.154, 0.63, length = 100 ) ) |> show
d = scan( starticle, 0.08, adaptive = true, dt = 1e-3, callback = VesicleForms.r_callback ) |> show
d = scan( starticle, 0.076, adaptive = false, dt = 1e-4, maxiters = 1000000 ) |> show
## new vesicles
starticle.parameters[3] = 0.604 # new u0
sol = ODEsolve( starticle, (0.0,2.0); callback = VesicleForms.r_callback, adaptive = true )
starticle.parameters[5] * sol.t[end] # new S
sol.u[end][3] # new u1
