using VesicleForms
using ProgressMeter
using BenchmarkTools
using OrdinaryDiffEq
using LaTeXStrings
using Plots
gr( fmt = :svg )
using LineSearches
using Statistics
using LsqFit

tspan = ( 0.0, 1.0 )

function scan_m( vesicle0, ms;
    gif = true,
    factor = 1.0,
    n_restarts = 10,
    ftol = 1e-8,
    res_options = NamedTuple(),
    kwargs...
    )
    vesicle01 = copy( vesicle0 )
    ms /= sqrt( vesicle0.area / 4pi )
    solutions = ODESolution[]
    progress = Progress( length(ms), desc="Increasing m..." )
    for m in ms
        vesicle01 = Vesicle( vesicle01, spont_curvature = m )
        restarts = 0
        old_parameters = copy(vesicle01.parameters)
        old_factor = copy(factor)
        while !((opt = VesicleForms.ODEshoot_root!( vesicle01, tspan; factor = factor, ftol = ftol, res_options = res_options, kwargs... )).f_converged)
            if restarts > n_restarts
                @error "All hope is lost. Exiting at m = $m..."
                show( IOContext(stdout, :compact => false), vesicle01 )
                solution = VesicleForms.solve_shapeEquations( vesicle01, tspan )[1]
                push!( solutions, solution )
                return solutions
            end # if
            restarts += 1
            @info "Cannot converge at $(old_parameters).\n Endet at $(vesicle01).\n Seek new starting point. ($(restarts))"
            vesicle01.parameters .= old_parameters
            vesicle01.parameters[4] = vesicle01.parameters[3]
            factor *= 0.9
            ftol = opt.residual_norm + 10eps()
            @info "New factor is $factor."
            @info "New ftol is $ftol."
            # factor = 2
            # parameterRanges = [ ( x>=0 ? (x/factor,x*factor) : (x*factor,x/factor)) for x in vesicle01.parameters ]
            # opt = VesicleForms.ODEshoot_opt!( vesicle01, tspan, parameterRanges )
            # vesicle01.parameters .= best_candidate( opt )
        end # while
        # factor = old_factor
        solution = VesicleForms.solve_shapeEquations( vesicle01, tspan )[1]
        push!( solutions, solution )
        ProgressMeter.next!( progress, showvalues = Ref((:m,vesicle01.spont_curvature)) )
    end
    if gif
        create_gif( solutions )
    end # if
    return solutions
end # function

function create_gif( solutions )
   anim = Animation()
   solution = first( solutions )
   p = solution.prob.p
   pl = plotSolution( solution;
    verbose = false,
    title=latexstring("\\bar{m}=$(p.m * sqrt(p.A / 4pi))"),
   )
   xmax = pl.series_list[1][:x] |> maximum
   ymax = pl.series_list[1][:y] |> maximum
   @showprogress "Creating gif..." for solution in solutions[2:end]
       p = solution.prob.p
       plotSolution( solution;
        verbose = false,
        title=latexstring("\\bar{m}=$(p.m * sqrt(p.A / 4pi))"),
        xlims = (0,xmax),
        ylims=(0,2ymax)
       )
       frame(anim)
   end
   Plots.gif( anim, fps = 15 )
end

function linear_fit( xdata, ydata, initial_p )
    @. model( x, p ) = p[1] + x * p[2]
    fit = curve_fit( model, xdata, ydata, initial_p )
    return fit, x->model(x,fit.param)
end # function

function plot_m_deps( solutions, nfitpoints = 10 )
    area = solutions[1].prob.p.A
    Rve = sqrt( area / 4pi )
    ms = (x->x.prob.p.m).(solutions)
    BnNs = VesicleForms.belly_n_neck_value.( solutions )
    x = ms.*Rve
    energies = (x->maximum(x[7,:])).(solutions)
    necks = getindex.( BnNs, 2 )./Rve
    bellies = getindex.( BnNs, 1 )./Rve
    co_bellies = getindex.( BnNs, 3 ) ./ Rve
    idxs = Int[]
    for i in eachindex( necks )
        if abs( bellies[i] - co_bellies[i] ) > 0.01
            continue
        end # if
        push!( idxs, i )
    end # for
    necks = necks[idxs]
    bellies = bellies[idxs]
    x = x[idxs]
    energies = energies[idxs]
    # plot( bellies - co_bellies, title=" Belly diff") |>display
    necks_fit, necks_model = linear_fit( x[end-nfitpoints:end], necks[end-nfitpoints:end], [necks[1],(necks[end]-necks[end-1]) / (x[end] - x[end - 1])] )
    belly_fit, belly_model = linear_fit( x[end-nfitpoints:end], bellies[end-nfitpoints:end], [bellies[1],(bellies[end]-necks[end-1]) / (x[end] - x[end - 1])] )
    # fit the whole thing
    @. model_t( x, p ) = p[1] + p[2] * x + p[3] * x^2 + p[4] * x^3 + p[5] * x^4 + p[6] * x^5
    @. model_inv( x, p ) = p[1] * (abs(x - p[2]))^p[3] + p[4]
    whole_fit = curve_fit( model_t, x, necks, [0.5,1.5,-0.5,1.0,1.0,1.0] )
    # whole_fit2 = curve_fit( model_inv, x, necks, [0.5,1.5,-0.5,1.0] )
    @info "Taylor fit parameters:\n"*
        "\tNeck: $(whole_fit.param)±$(stderror(whole_fit))\n"*
        "\tValue at sqrt(2): $(model_t(sqrt(2),whole_fit.param))"
    # @info "Power fit parameters:\n"*
    #     "\tNeck: $(whole_fit2.param)±$(standard_error(whole_fit2))\n"*
    #     "\tValue at sqrt(2): $(model_inv(sqrt(2),whole_fit2.param))"
    pb = plot( x, bellies, lab="Bellies radius vs m (unitless)", ylims=extrema(bellies), legend = :bottomright )
    plot!( pb, x, belly_model.(x), lab="Linear fit" )
    pn = plot( x, necks, lab="Neck radius vs m (unitless)", ylims=extrema(necks) )
    plot!( pn, x, necks_model.(x), lab="Linear fit" )
    plot!( pn, x, model_t.(x, Ref(whole_fit.param)), lab="Taylor fit" )
    # plot!( pn, x, model_inv.(x, Ref(whole_fit2.param)), lab="Whole fit" )
    @info "Parameters of linear fit:\n"*
        "\tBelly: $(belly_fit.param)±$(stderror(belly_fit))\n"*
        "\tNeck: $(necks_fit.param)±$(stderror(necks_fit))\n"
    pe = plot( x, energies, lab="Energie vs m (unitless)"
            # , yscale = :log10
        )
    return pb, pn, pe
end # function
## creates gif
# vesicle0 = VesicleForms.prolate0_685 |> copy
vesicle0 = VesicleForms.prolate_ideal_neck |> copy
# vesicle0 = VesicleForms.prolate_sqrt2_m_max |> copy
# vesicle0 = VesicleForms.prolate0_685m_max |> copy
pp_osc = 500
m_max = VesicleForms.prolate_sqrt2_m_max.spont_curvature * sqrt( VesicleForms.prolate_sqrt2_m_max.area / 4pi )
# m_max = sqrt(2) - 0.01
# m_max = 1.2
m_min = vesicle0.spont_curvature * sqrt( vesicle0.area / 4pi )
m_range = range( m_min, m_max, length = pp_osc ÷ 2 )
oscillations = 1
linear_m = repeat( [collect(m_range); collect(reverse(m_range))], oscillations )
amp = (m_max - m_min) / 2
sin_m = -amp * cospi.( range(0.,1.0, length = pp_osc ÷ 2) ) .+ (m_min + m_max) / 2
resicle = scan_m( vesicle0,
        m_range;
        gif = false,
        # sin_m;
        res_options = ( force_symmetry = true, ),
        ftol = 1e-12,
        factor = 1.0,
        # n_restarts = 50,
        autoscale = true,
        # show_trace = true,
        # extended_trace = true,
        # method = :newton,
        # linesearch = LineSearches.HagerZhang( linesearchmax = 100 )
        # linesearch = LineSearches.MoreThuente()
    )
vesicle = VesicleForms.Vesicle( last(resicle).prob.p )
##
( dep_plots = plot_m_deps(resicle[1:end-1], 10) ) .|> display
plotSolution.(resicle; verbose = false) .|> display
for prob in resicle
    show( IOContext(stdout, :compact=>false), prob.prob.p )
    println()
end

## shows plots
resicle = scan_m( vesicle0, 0.0:0.01:2.0; gif = false )
plotSolution.( resicle )

##
using OrdinaryDiffEq
oscillations = 1
Base.reverse( s::ODESolution ) = ODESolution( reverse!(s.u), s.u_analytic, s.errors, reverse!(s.t), s.k, s.prob, s.alg, s.interp, s.dense, s.tslocation, s.retcode )
p = create_gif( repeat( [resicle; reverse(resicle)], oscillations ) )

## DEBUG
scale = sqrt( vesicle0.area / 4pi )
scale_m = [ 1., scale, 1/scale, 1/scale^2, 1/scale^3 ]
vesicle1 = VesicleForms.Vesicle( vesicle0, m = (m_range / scale )[2] )
sol = VesicleForms.solve_shapeEquations( vesicle0, tspan )
sol[2][[1,2,3,5,6]] .* scale_m
cache = zeros(5)
VesicleForms.root_residualAV!( cache, vesicle1.parameters[[1,2,3,5]], vesicle1, tspan; force_symmetry = true )
cache
VesicleForms.ODEshoot_root!( vesicle1, tspan, factor = 0.001; res_options = ( force_symmetry = true, ) )

## DEBUG Instability
collectedvesicle = (p = [-24.9054, 7.33639, 1.96236, 1.96236, 3.12807], m = 1.6295930339164932, A = 6.092947785379556, V = 1.0, κ = 8.0e-20)
instabilicle = VesicleForms.Vesicle( collectedvesicle )
sol, du = VesicleForms.solve_shapeEquations( instabilicle, tspan )
##
VesicleForms.plotSolution( sol,
    xlims = (0.,1.5), ylims = (0.,2e9)
)
sol[2,end-250:end-100] |> show
