using VesicleForms
using LaTeXStrings
using OrdinaryDiffEq
using LineSearches

tspan = ( 0.0, 1.0 )

# vesicle0 = VesicleForms.prolate_ideal_neck |> copy
# pp_osc = 1000
vesicle0 = VesicleForms.prolate0_685 |> copy
pp_osc = 1000
# vesicle0 = VesicleForms.prolate_ideal_neck_m_max |> copy
# pp_osc = 40
# m_max = VesicleForms.prolate_sqrt2_m_max.spont_curvature * sqrt( VesicleForms.prolate_sqrt2_m_max.area / 4pi )
# m_max = sqrt(2) - 0.01
m_max = 2
# m_max = 1.1
m_min = vesicle0.spont_curvature * sqrt( vesicle0.area / 4pi )
m_range = range( m_min, m_max, length = pp_osc ÷ 2 )
oscillations = 1
linear_m = repeat( [collect(m_range); collect(reverse(m_range))], oscillations )
amp = (m_max - m_min) / 2
sin_m = -amp * cospi.( range(0.,1.0, length = pp_osc ÷ 2) ) .+ (m_min + m_max) / 2
resicle = VesicleForms.scan_m( vesicle0,
        m_range;
        # sin_m;
        res_options = ( force_symmetry = false, dt = 1e-3 ),
        # solver = :minpack,
        # tol =,
        # method = :hybrd,
        solver = :nlsolve,
        ftol = 1e-12,
        autodiff = :finite,
        # method = :newton,
        # linesearch = LineSearches.HagerZhang( linesearchmax = 100 ),
        # factor = 1.0,
        # n_restarts = 50,
        # autoscale = true,
        # show_trace = true,
        # extended_trace = true,
    )
@info "Final m̄: $(VesicleForms.Vesicle(last(resicle)).m̄)"
## DEBUGGING
@debug begin
    goodicle = VesicleForms.solution_to_vesicle(resicle[end-1])
    dm = resicle[end].prob.p.m - resicle[end-1].prob.p.m
    problemicle = VesicleForms.Vesicle( goodicle, spont_curvature = resicle[end].prob.p.m + dm
      )
    initialConditionsAV( collect( problemicle ) )
    endConditionsAV( collect( problemicle ), -0.01 )
    VesicleForms.plotVesicle( goodicle, verbose = false )
    VesicleForms.plotVesicle( problemicle, verbose = false )
    tspan = (0.0,1.0)
    problemicle.parameters[4] = problemicle.parameters[3]
    opt = VesicleForms.ODEshoot_root!( problemicle, tspan, factor = 1.0, ftol = 2e-4 )
    ##
    VesicleForms.plotVesicle( problemicle, verbose = false )
    cache = zeros(5)
    rooticle = VesicleForms.sphere(); VesicleForms.root_residualAV!( cache, rooticle.parameters, rooticle, tspan )
    "cache = $cache"
end
