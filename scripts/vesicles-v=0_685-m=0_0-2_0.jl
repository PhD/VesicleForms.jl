using VesicleForms

tspan = ( 0.0, 1.0 )

vesicle0 = VesicleForms.prolate0_685 |> copy
nves = 500
m_max = 2
m_min = vesicle0.spont_curvature * sqrt( vesicle0.area / 4pi )
m_range = range( m_min, m_max, length = nves )
resicles = VesicleForms.scan_m( vesicle0,
        m_range;
        res_options = ( force_symmetry = false, dt = 1e-3 ),
        solver = :nlsolve,
        ftol = 1e-12,
        autodiff = :finite,
    )
res_vesicles = VesicleForms.solution_to_vesicle.(resicles)
