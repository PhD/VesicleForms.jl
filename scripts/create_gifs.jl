function gif_scan( vesicle, u0_range; fps = 5, kwargs... )
    @gif for u0 in u0_range
        vesicle.parameters[3] = u0
        sol = ODEsolve( vesicle, (0.0,2.0); callback = VesicleForms.r_callback, adaptive = true, kwargs... )
        @show sol.u[end][1]-pi, sol.u[end][2]
        sol.retcode == :SweetSpot && @info "Found a sweet spot!"
    end # for
    # gif(anim, homedir(), fps = fps )
end # function
