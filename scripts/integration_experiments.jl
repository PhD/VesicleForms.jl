using OrdinaryDiffEq, DiffEqCallbacks
using VesicleForms
using Plots

function bulk_shapeEquations(newState, state, parameters, time)
    P, Σ, v0, v1, S =  parameters.p
    φ,r,v,γ = state
    m = parameters.m
    sinPhi, cosPhi = sincos( φ )
    sin2Phi = sinPhi * sinPhi
    newState[1] = S * v
    newState[2] = S * cosPhi
    newState[3] = S * ( ( cosPhi * sinPhi / r + γ * sinPhi - v * cosPhi ) / r + P * r * cosPhi / 2 ) # u = \dot{φ}
    newState[4] = S * ( ( ( v - 2m )^2 - sin2Phi / r^2 ) / 2 + P * r * sinPhi + Σ ) # γ
    newState[5] = S * 2pi * r # A
    newState[6] = S * pi * sinPhi * r^2 # V
    newState[7] = S * pi * r * ( v + sinPhi/r - 2m )^2 # bending Energy
    return nothing
end

function small_r_shapeEquations(newState, state, parameters, time; out = true)
    P, Σ, v0, v1, S =  parameters.p
    φ,r,v,γ = state
    m = parameters.m
    sinPhi, cosPhi = sincos( φ )
    newState[1] = S * v
    newState[2] = S * cosPhi
    newState[5] = S * 2pi * r                    # A
    newState[6] = S * pi * sinPhi * r^2          # V
    if r == zero(r)
        newState[3] = S * v * γ                  # u = \dot{φ}
        newState[4] = S * ( 2(m^2 - m * v) + Σ ) # γ
        newState[7] = S * 4pi * r * ( v - m )^2  # bending Energy
    else
        if out
            ds = S * time
            sinPhiByR = v0 + 1/6 * (v0 * (1-v0^2) + 3/8 * ( P + 2v0 * ( 2m^2 + Σ - 2m * v0 ) ) ) * ds^2
        else
            ds = S * abs( time - 1 )
            sinPhiByR = v1 + 1/6 * (v1 * (1-v1^2) + 3/8 * ( P + 2v1 * ( 2m^2 + Σ - 2m * v1 ) ) ) * ds^2
        end # if
        newState[3] = S * (  γ * sinPhiByR + P * r * cosPhi / 2 ) # cosPhi / r * (sinPhiByR - v) + # u = \dot{φ}
        newState[4] = S * ( ( ( v - 2m )^2 - sinPhiByR^2 ) / 2 + P * r * sinPhi + Σ ) # γ
        newState[7] = S * pi * r * ( v + sinPhiByR - 2m )^2 # bending Energy
    end # if
end # function

function get_shape(vesicle, alg = Vern9(), tols = (:abstol => 1e-12, :reltol => 1e-12); solveargs... )
    tspan=(0.0,1.0)
    solveit = (problem; kwargs...)->solve(problem, alg; tols..., kwargs...)
    parameters = collect(vesicle)
    start_condition = function( u, t, integrator; tol = eps() )
        tol - abs(u[2] - VesicleForms.initialConditions( t, integrator.sol.prob.p )[2])
    end # function
    end_condition = function( u, t, integrator; tol = 1e-10 )
        tol - abs(u[2] - VesicleForms.rightBoundary( t - tspan[end], integrator.sol.prob.p )[2])
    end # function
    start_callback = ContinuousCallback( start_condition, terminate! )
    end_callback = ContinuousCallback( end_condition, terminate! )

    start_prob = ODEProblem( small_r_shapeEquations, initialConditionsAV(parameters), tspan, parameters )
    solution = solveit( start_prob; callback = start_callback, solveargs... )

    mid_prob = ODEProblem( bulk_shapeEquations, solution.u[end], (solution.t[end],tspan[end]), parameters)
    solution2 = solveit( mid_prob; callback = end_callback, solveargs... )

    end_prob = ODEProblem( (du,u,p,t)->small_r_shapeEquations(du,u,p,t; out = false), solution2.u[end], (solution2.t[end],tspan[end]), parameters)
    solution3 = solveit( end_prob, solveargs... )

    if solution.retcode == :Terminated
        push!(solution.u, solution2.u...)
        push!(solution.t, solution2.t...)
    elseif solution2.retcode == :Termianted
        push!(solution.u, solution3.u...)
        push!(solution.t, solution3.t...)
    end

    return solution
end # function

sol = get_shape( VesicleForms.sphere() ) |> display
