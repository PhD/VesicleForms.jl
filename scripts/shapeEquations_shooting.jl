using VesicleForms
# using Plots
# ## setup
# gr()
# # pgfplots()
const tstart = 0.0#1e-3
# const tstart = 0.
tspan = ( tstart, 1.0-tstart )

initisicle = copy( VesicleForms.sphere() )
critisicle = copy( VesicleForms.critSphere )

##
ODEmatch( critisicle, tspan )
ODEmatch( VesicleForms.prolate0_7, tspan )
ODEmatch( VesicleForms.oblate0_9, tspan )

## test sphere
critisicle = copy( VesicleForms.critSphere )
cache = zeros(5)
root_match!( cache, critisicle.parameters, critisicle, tspan )
cache
ODEshoot_root!( initisicle, tspan, root_match! )
critisicle
sphere
ODEmatch( initisicle, tspan )
ODEmatch( critisicle, tspan )

## change v
critV = VesicleForms.set_v!( critisicle, 0.9 )
@timev ODEshoot_root!( critV, tspan, VesicleForms.root_residualAV! )
@timev ODEmatch( critV, tspan )

## continue
critV = VesicleForms.set_v!( critV, critV.red_volume - 0.05 )
ODEshoot_root!( critV, tspan, root_match! )
ODEmatch( critV, tspan )

## inspection
cache = zeros(5)
root_match!( cache, critV.parameters, critV, tspan )
cache

## relax to v
critisicle = copy( VesicleForms.critSphere )
critisicle = relax_to_v( critisicle, 0.7, tspan, -0.01 )

## inspection
cache = zeros(5)
root_match!( cache, critisicle.parameters, critisicle, tspan )
cache

## relax to v ( oblate / prolate )
chiller = copy( prolate0_7 )
chiller = relax_to_v( chiller, 0.652, (1e-3,1-1e-3)
                        , -0.001
                    )
chiller = copy( oblate0_7 )
chiller = relax_to_v( chiller, 0.652, (1e-3,1-1e-3)
                        , -0.001
                    )

## inspection
cache = zeros(5)
root_match!( cache, chiller.parameters, chiller, tspan )
cache
