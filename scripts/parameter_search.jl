using VesicleForms
using Plots

v_start = VesicleForms.prolate_ideal_neck |> copy
P, Σ, u₀ = v_start.parameters
ppp = 100 # points per parameter
Ps = range( 0.5P, stop = 2.0P, length = ppp )
Σs = range( 0.5Σ, stop = 2.0Σ, length = ppp )
u0s = range( 0.5u₀, stop = 2.0u₀, length = ppp )
# u0s = collect(u0s)
# uind = findfirst( u->u>= 5.24504 - 0.1, u0s )
# u0s = u0s[uind:end]
vs = VesicleForms.findshapes( v_start, Ps, Σs, u0s, (0.0,2.0), adaptive = false )
##
for (i1, va) in vs |> enumerate
    for (i2, v) in va |> enumerate
        VesicleForms.plotVesicle( v, title=latexstring("\\thinspace u_{0} = $(v.parameters[3]), \\Sigma = $(v.parameters[2]), \\mbox{id} = ($i1,$i2)") )
    end # for
end # for
