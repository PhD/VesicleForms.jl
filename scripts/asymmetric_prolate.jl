using VesicleForms
using OrdinaryDiffEq, DiffEqCallbacks
using LaTeXStrings

p = -1
m = 1.27 / 2 * abs(p)^(1/3)
Σ = 0.815 * abs(p)^(2/3)
Σ = 0.800 * abs(p)^(2/3)
u₀ = 5//8 * abs(p)^(1/3)
neck = VesicleForms.Vesicle( m, 1.0, 1.0, [p,Σ,u₀,π] )
u0s = range( 0.5u₀, stop = 2.0u₀, length = 100 )
Σs = range( 0.7*abs(p)^(2/3), stop = 0.85*abs(p)^(2/3), length = 10 )
vs = VesicleForms.findshapes( neck, Σs, u0s, (0.0,2.0) )
for (i1, va) in vs |> enumerate
    for (i2, v) in va |> enumerate
        VesicleForms.plotVesicle( v, title=latexstring("\\thinspace u_{0} = $(v.parameters[3]), \\Sigma = $(v.parameters[2]), \\mbox{id} = ($i1,$i2)") )
    end # for
end # for
# VesicleForms.relation_diff.( vs )
##
VesicleForms.scan( neck, range(0.5,stop=0.53,length=50) ) |> show
## oldstuff
ϵ = 1e-3
ϵ = 0.0
tspan = (0.0+ϵ,1.0-ϵ)

neck.parameters[3] = 0.7985
sol = ODEsolve( neck, (0.0,10.0); callback = VesicleForms.r_callback, adaptive = true )



shapes = VesicleForms.PRAshot!( starticle, tspan );
plotSolution.( shapes )
