using VesicleForms
##ß
proto = VesicleForms.right_pear
VesicleForms.plotSymmetryBreaking( proto )
##
sol = VesicleForms.plotVesicle( proto )
ylims!(0,3)
title!("Pear (right)")
savefig("/usr/home/christ/Nextcloud/Documents/PhD/MMg/Graphics/ShapeEquations/right_pear.png")
