using VesicleForms


analytic_sphere = VesicleForms.sphere( volume = 1.#460.0
    )
sphere1 = copy( analytic_sphere )
initisicle = copy( VesicleForms.prolate0_9 )
critisicle = copy( VesicleForms.critSphere )
dt = 1e-3
dt = 0.0
tspan = (dt,1.0)
##
res = VesicleForms.ODEshoot_opt!( copy(VesicleForms.prolate0_9), tspan, [(1e-3,1e2),(-1e2,-1e-3),(1e-1,1e1),(1e-1,1e1),(1e-1,1e2)] )
##
sphere1 = copy( analytic_sphere )
o = ODEshoot_root!( initisicle, tspan )
VesicleForms.plotVesicle( sphere )
##
ODEshoot_root!( initisicle, tspan, res_options = (auto_m = true,), iterations = 3000, ftol = 1e-10 ).f_converged

## relax to v
resicle = relax_to_v( initisicle, 0.9, tspan;
        ftol = 1e-12,
        factor = 1.0,
        iterations = 150,
        res_options = ( auto_m = true, )
        # residual = VesicleForms.root_fullmatch!
    )
# sol = VesicleForms.solve_shapeEquations( VesicleForms.sphere, (tspan[1],1tspan[end]); callback = VesicleForms.r_callback )
# sol = VesicleForms.solve_shapeEquations( sphere, (tspan[1],1tspan[end]); callback = VesicleForms.r_callback )
# ODEsolve( initisicle, (0.0,1.0) )
@info "Global shape relation: $(VesicleForms.relation_diff( resicle ))"
##
plotSolution( vesicle_to_shape( resicle, tspan ) )
plotSolution( vesicle_to_shape( resicle, tspan ), gas(0), 1. )

## append to file
open( "src/vesicle_prototypes.jl", "a" ) do io
    println( IOContext(io, :compact => false), "prolate0_7 = ", resicle )
end
